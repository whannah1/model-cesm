#!/usr/bin/env python
#========================================================================================================================
# 	This script runs an ensemble of DYNAMO hindcast simulations with SP-CAM	
#
#    Sep, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================================
#========================================================================================================================
#yrmndy = 20110000 + [1001,1006,1011,1016,1021,1026,1031,1105,1110,1115,1120,1125,1130,1205,1210,1215]
#yrmndy = 20110000 + np.array([1205])

yrmndy = 20110000 + np.array([1001,1006])
	
member = "92"

ndays = 10

newcase = True
config  = True
build   = True
runsim  = False

res         = "f09"
member_stub = "DYNAMO_"+member+"_"+res+"_"

#srcmod_dir  = "/glade/u/home/whannah/CESM/src_mod/"
nmlmod_dir  = "/glade/u/home/whannah/CESM/nml_mod/"
newcase_cmd = "/glade/u/home/whannah/CESM/cesm1_1_1_spcam/scripts/create_newcase"
#===================================================================================
#===================================================================================
yr = np.floor( yrmndy/10000 )
mn = np.floor((yrmndy - yr*10000)/100)
dy = yrmndy - yr*10000 - mn*100

num_t = len(yrmndy)  
#===================================================================================
#===================================================================================
print
for t in range(0,num_t):
	yrstr = "%04i" % yr[t] 
	mnstr = "%02i" % mn[t]
	dystr = "%02i" % dy[t]
	os.system("cd ~/CESM")
	case_name = member_stub+str(yrmndy[t])
	case_dir  = "/glade/u/home/whannah/CESM/"+case_name+"/"
	cdcmd = "cd "+case_dir+" ; "
	data_filename = "/glade/u/home/whannah/DYNAMO_IC_wSST/CAM_FV09.cam2.i."+yrstr+"-"+mnstr+"-"+dystr +"-00000.nc"
	print "  case : "+case_name
	print
	#--------------------------
	# Create new case
	#--------------------------
	if newcase == True:
		cmd = newcase_cmd+" -case "+case_dir+"  -compset  F_2000_SPCAM_sam1mom  -res  "+res+"_"+res+" -mach  yellowstone"
		os.system(cmd)
	#--------------------------
	# Configure the case
	#--------------------------
	if config == True:
		start_date = yrstr+"-"+mnstr+"-"+dystr
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE	-val "+start_date)
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_START    -val 2011")
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_END      -val 2012")
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_DATA_FILENAME -val "+data_filename)
		os.system(cdcmd+"./xmlchange -file env_build.xml -id CAM_CONFIG_OPTS -val \"-rad camrt -chem trop_bam -phys cam4 -use_SPCAM 		\
		                                                                            -crm_nx 32 -crm_ny 1 -crm_nz 28 -nlev 30 -crm_dx 4000 	\
		                                                                            -crm_dt 40 -SPCAM_microp_scheme sam1mom\" ")
		os.system(cdcmd+"./cesm_setup")
	#--------------------------
	# Build the model
	#-------------------------
	if build == True:
		os.system(cdcmd+"./"+case_name+".clean_build")			# Clean previous build
		#os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")	# copy the modified source code
		srcnml = nmlmod_dir+"user_nl_cam."+member
		desnml = case_dir+"user_nl_cam"
		ifile = open(srcnml, 'r')
		ofile = open(desnml, 'w')
		for line in ifile: 
			ofile.write( line.replace("ncdata", " ncdata = '"+data_filename+"'") )
		os.system(cdcmd+"./xmlchange -file env_run.xml -id STOP_N -val "+str(ndays))	# modify the run settings
		os.system(cdcmd+"./"+case_name+".build")				# Build the model
	#--------------------------
	# Run the simulation
	#--------------------------
	if runsim == True:
		runfile = case_dir+case_name+".run"
		subfile = case_dir+case_name+".submit"
		#os.system("sed -i '/#BSUB -R \"select*/d'  "+runfile)	# What is this for? -WH
		os.system(cdcmd+subfile)
	#--------------------------
	#--------------------------
	del case_dir


