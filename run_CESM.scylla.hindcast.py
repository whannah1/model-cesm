#!/usr/bin/env python
#========================================================================================================================
# 	This script runs an ensemble of DYNAMO hindcast simulations with SP-CAM	
#
#    Sep, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import os
import numpy as np
home = os.getenv("HOME")
#========================================================================================================================
#========================================================================================================================
#yrmndy = 20110000 + [1001,1006,1011,1016,1021,1026,1031,1105,1110,1115,1120,1125,1130,1205,1210,1215]
#yrmndy = 20110000 + np.array([1205])

yrmndy = 20110000 + np.array([1001])
	
member = "00"

ndays = 2

newcase = True
config  = True
build   = True
runsim  = False

res         = "ne30"
member_stub = "UNICON"+"_TEST_"+member+"_"+res#+"_"
#member_stub = "CLUBB"+"_TEST_"+member+"_"+res#+"_"

top_dir 	= "/home/whannah/Model/CESM/"
srcmod_dir	= top_dir+"src_mod/"
nmlmod_dir  = top_dir+"nml/"
newcase_cmd = top_dir+"UNICON_e13b7u1.8/scripts/create_newcase"
#newcase_cmd = top_dir+"clubbMG2_n36_cam5_3_08/scripts/create_newcase"
#===================================================================================
#===================================================================================
yr = np.floor( yrmndy/10000 )
mn = np.floor((yrmndy - yr*10000)/100)
dy = yrmndy - yr*10000 - mn*100

num_t = len(yrmndy)  
#===================================================================================
#===================================================================================
print
for t in range(0,num_t):
	yrstr = "%04i" % yr[t] 
	mnstr = "%02i" % mn[t]
	dystr = "%02i" % dy[t]
	os.system("cd "+top_dir)
	case_name = member_stub #+str(yrmndy[t])
	case_dir  = top_dir+"Cases/"+case_name+"/"
	cdcmd = "cd "+case_dir+" ; "
	data_filename = "~/Model/DYNAMO_IC_wSST/CAM_FV09.cam2.i."+yrstr+"-"+mnstr+"-"+dystr +"-00000.nc"
	print "  case : "+case_name
	print
	#--------------------------
	# Create new case
	#--------------------------
	if newcase == True:
		cmd = newcase_cmd+" -case "+case_dir+"  -compset  F_2000  -res  "+res+"_"+res+" -mach  userdefined"
		print cmd
		os.system(cmd)
		print 

		os.system(cdcmd+"./xmlchange -file env_build.xml 	-id OS					-val darwin ")
		os.system(cdcmd+"./xmlchange -file env_build.xml 	-id MPILIB				-val openmpi ")
		os.system(cdcmd+"./xmlchange -file env_build.xml 	-id COMPILER			-val gnu")
		os.system(cdcmd+"./xmlchange -file env_build.xml 	-id EXEROOT				-val ~/Model/CESM/"+case_name+"/bld ")
		os.system(cdcmd+"./xmlchange -file env_run.xml 		-id RUNDIR				-val ~/Model/CESM/"+case_name+"/run")
		os.system(cdcmd+"./xmlchange -file env_run.xml 		-id DIN_LOC_ROOT		-val ~/Model/CESM/inputdata/")
		os.system(cdcmd+"./xmlchange -file env_mach_pes.xml	-id MAX_TASKS_PER_NODE	-val 1")
	#--------------------------
	# Configure the case
	#--------------------------
	if config == True:
		start_date = yrstr+"-"+mnstr+"-"+dystr
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE	-val "+start_date)
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_START    -val 2011")
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_END      -val 2012")
		os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_DATA_FILENAME -val "+data_filename)
		os.system(cdcmd+"./cesm_setup -clean")
		os.system(cdcmd+"./cesm_setup")
	#--------------------------
	# Build the model
	#-------------------------
	if build == True:
		os.system(cdcmd+"./"+case_name+".clean_build")			# Clean previous build
		#os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")	# copy the modified source code
		srcnml = nmlmod_dir+"user_nl_cam."+member
		desnml = case_dir+"user_nl_cam"
		ifile = open(srcnml, 'r')
		ofile = open(desnml, 'w')
		for line in ifile: 
			ofile.write( line.replace("ncdata", " ncdata = '"+data_filename+"'") )
		os.system(cdcmd+"./xmlchange -file env_run.xml -id STOP_N -val "+str(ndays))	# modify the run settings
		os.system(cdcmd+"./"+case_name+".build")				# Build the model
	#--------------------------
	# Run the simulation
	#--------------------------
	if runsim == True:
		runfile = case_dir+case_name+".run"
		subfile = case_dir+case_name+".submit"
		#os.system("sed -i '/#BSUB -R \"select*/d'  "+runfile)	# What is this for? -WH
		os.system(cdcmd+subfile)
	#--------------------------
	#--------------------------
	del case_dir

