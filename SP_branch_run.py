#!/usr/bin/env python
#========================================================================================================================
#   This script runs an ensemble of Single-Colum CAM (SCAM) simulations
#
#    Mar, 2015  Walter Hannah       University of Miami
#
# If a segmentation fault is encountered at runtime first try using 'ulimit -s 65532' to increase the stack size
#========================================================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#========================================================================================================================
#========================================================================================================================
newcase,config,build,copyinit,runsim = False,False,False,False,False

#newcase  = True
#config   = True
#build    = True
copyinit = True
runsim   = True

case_num = "00"
res      = "0.9x1.25_gx1v6"
cld      = "ZM"      #  ZM / SP

ndays   = 5

exp = "CTL"    # CTL / EXP
mod = "CPL"    # CPL / SST / C02

case_name = "CESM_"+cld+"_"+exp+"-"+mod+"_"+res+"_"+case_num

#===================================================================================
#===================================================================================
top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/"

run_dir = "/glade/scratch/whannah/"
src_dir = "~/Model/CESM/CESM_SRC/cesm1_1_1_spcam/"
#===================================================================================
#===================================================================================
os.system("cd "+top_dir)
case_dir  = top_dir+"Cases/"+case_name+"/"
cdcmd = "cd "+case_dir+" ; "
print
print "  case : "+case_name
print
#--------------------------
# Create new case
#--------------------------
if newcase == True:
    compset_opt = "-compset B_1850_CAM5_CN"
    newcase_cmd = src_dir+"scripts/create_newcase"
    cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+" -mach  yellowstone "
    print cmd
    os.system(cmd)
#--------------------------
# Configure the case
#--------------------------
if config == True:
    # set run-time variables
    if exp=="CTL" : start_date = "1900-01-01"
    if exp=="CTL" : start_date = "2100-01-01"
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE  -val "+start_date)
    #------------------------------------------------
    # Copy the custom namelist file
    #------------------------------------------------
    os.system("cp "+nmlmod_dir+"user_nl_cam  "+case_dir+"user_nl_cam ")
    #------------------------------------------------
    # configure the case
    #------------------------------------------------
    os.system(cdcmd+"./cesm_setup -clean")
    os.system(cdcmd+"./cesm_setup")
#--------------------------
# Build the model
#--------------------------
if build == True:
    os.system(cdcmd+"./"+case_name+".clean_build")                  # Clean previous build
    #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
    os.system(cdcmd+"./"+case_name+".build")                        # Build the model
#--------------------------
# Copy init data
#--------------------------
if copyinit == True:
    if exp=="CTL" : branch_data = "/glade/p/work/whannah/spcesm_cam5_control/1889-01-07-00000/* "
    if exp=="EXP" : branch_data = "/glade/p/work/whannah/spcesm_cam5_4xCO2/2195-01-27-00000/* "
    os.system("cp "+branch_data+" "+run_dir+case_name+"/run")
    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_TYPE    -val hybrid")
    os.system(cdcmd+"./xmlchange -file env_run.xml     -id GET_REFCASE -val FALSE")
    if exp=="CTL":
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFCASE -val b40.1850.track1.1deg.006")
        #os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFCASE -val spcesm_cam5_control")
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFDATE -val 1889-01-01")
    if exp=="EXP":
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFCASE -val spcesm_cam5_4xCO2")
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFDATE -val 2195-01-27")
#--------------------------
# Run the simulation
#--------------------------
if runsim == True:
    runfile = case_dir+case_name+".run"
    subfile = case_dir+case_name+".submit"
    
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N     -val "+str(ndays)) 
    #os.system(cdcmd+"./xmlchange -file env_run.xml   -id RESUBMIT  -val 1")
    
    os.system("sed -i '/#BSUB -R \"select*/d'  "+runfile)
    os.system("sed -i '/#BSUB -W/ c\#BSUB -W 6:00'  "+runfile)
    
    os.system(cdcmd+subfile)
#--------------------------
#--------------------------
del case_dir

