#!/usr/bin/env python
#========================================================================================================================
#   This script transfers output CESM data for a specified case to scylla.meas.ncsu.edu
#    June, 2015  Walter Hannah       North Carolina State University
#========================================================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#========================================================================================================================
#========================================================================================================================
mkdir,get_log,get_atm_h0,get_atm_h1,get_atm_h2,get_lnd_h0,get_ocn_h0,get_ocn_hs = False,False,False,False,False,False,False,False

case_num = "02"
res = "0.9x1.25_gx1v6"
cld = "ZM"      #  ZM / SP
exp = "EXP"     # CTL / EXP
mod = "CPL"     # CPL / SST / C02

case_name = "CESM_"+cld+"_"+exp+"-"+mod+"_"+res+"_"+case_num

#tcmd = "scp -p "
tcmd = "rsync --ignore-existing --times "

#mkdir      = True
#get_log    = True
#get_atm_h0 = True
get_atm_h1 = True
get_atm_h2 = True
#get_lnd_h0 = True
#get_ocn_h0 = True
#get_ocn_hs = True
#get_ocn_h = True
#===================================================================================
# set directory paths and destination server
#===================================================================================
dst_server = "scylla.meas.ncsu.edu"

src_dir = "/glade/scratch/whannah/archive/"+case_name+"/"
dst_dir = "~/Data/CESM/"+case_name+"/" 

print
print case_name
print
#===================================================================================
# Make sure destination directory exists
#===================================================================================
if mkdir :
    print "    creating destination directories..."
    os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"     '")
    os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"atm/ '")
    os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"lnd/ '")
    os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"ocn/ '")
    os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"logs/ '")
print
#===================================================================================
# copy the data
#===================================================================================
if get_log :
    print "copying log files..."
    cmd = tcmd+src_dir+"/*/logs/*   "+dst_server+":"+dst_dir+"logs/ "
    print ""+cmd
    os.system(cmd)

if get_atm_h0 :
    print "copying atm h0 data..."
    cmd = tcmd+src_dir+"atm/hist/*.h0.*.nc   "+dst_server+":"+dst_dir+"atm/ "
    print ""+cmd
    os.system(cmd)

if get_atm_h1 :
    print "copying atm h1 data..."
    cmd = tcmd+src_dir+"atm/hist/*.h1.*.nc   "+dst_server+":"+dst_dir+"atm/ "
    print ""+cmd
    os.system(cmd)
    
if get_atm_h2 :
    print "copying atm h2 data..."
    cmd = tcmd+src_dir+"atm/hist/*.h2.*.nc   "+dst_server+":"+dst_dir+"atm/ "
    print ""+cmd
    os.system(cmd)
    
if get_lnd_h0 :
    print "copying lnd h0 data..."
    cmd = tcmd+src_dir+"lnd/hist/*.h0.*.nc   "+dst_server+":"+dst_dir+"lnd/ "
    print ""+cmd
    os.system(cmd)
    
if get_ocn_h0 :
    print "copying ocn h0 data..."
    cmd = tcmd+src_dir+"ocn/hist/*.h0.*.nc   "+dst_server+":"+dst_dir+"ocn/ "
    print ""+cmd
    os.system(cmd)
    
if get_ocn_hs :
    print "copying ocn h1 data..."
    cmd = tcmd+src_dir+"ocn/hist/*.hs.*.nc   "+dst_server+":"+dst_dir+"ocn/ "
    print ""+cmd
    os.system(cmd)


if get_ocn_h :
    tsrc_dir = "/glade/scratch/whannah/"+case_name+"/"
    print "copying ocn h data..."
    cmd = tcmd+tsrc_dir+"ocn/*.h.*.nc   "+dst_server+":"+dst_dir+"ocn/ "
    print ""+cmd
    os.system(cmd)
    
    
#===================================================================================
#===================================================================================
print "done."
print
#===================================================================================
#===================================================================================
