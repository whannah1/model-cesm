#!/usr/bin/env python
#=============================================================================================
#   This script runs an ensemble of Single-Colum CAM (SCAM) simulations
#
#    Mar, 2015  Walter Hannah       University of Miami
#
# If a segmentation fault is encountered at runtime first 
# try using 'ulimit -s 65532' to increase the stack size
#=============================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#=============================================================================================
#=============================================================================================
newcase,config,build,runsim = False,False,False,False

#newcase = True
# config  = True
# build   = True
runsim  = True

member  = "99"
res     = "1.9x2.5" # ne16
cld     = "ZM"      #  CLUBB / UNICON / ZM

ndays =  1#365*5
cflag = "FALSE"
#===================================================================================
#===================================================================================
case_name = "AQUA_"+cld+"_"+member+"_"+res+"_004"

data_filename = home+"/Model/CESM/IC_AQUA/sst_aqua_ideal.v2.amp_0.0.xc_0.0.yc_4.0.nc"

top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/"

run_dir = home+"/Model/CESM/scratch/"
src_dir = home+"/Model/CESM/CESM_SRC/"
if cld == "ZM"     : src_dir = src_dir + "cesm1_2_2/"
if cld == "SP"     : src_dir = src_dir + "cesm1_2_3/"
# if cld == "UNICON" : src_dir = src_dir + "UNICON_e13b7u1.8/"
# if cld == "CLUBB"  : src_dir = src_dir + "clubbMG2_n36_cam5_3_08/"
newcase_cmd = src_dir+"scripts/create_newcase"

#yrmndy = 2000*10000 + np.array([ 0101 ])
yr = 0000   #np.floor( yrmndy/10000 )
mn = 1      #np.floor((yrmndy - yr*10000)/100)
dy = 1      #yrmndy - yr*10000 - mn*100

num_t = 1 #len(yrmndy)  
#===================================================================================
#===================================================================================
print
for t in range(0,num_t):
    yrstr = "%04i" % yr#[t] 
    mnstr = "%02i" % mn#[t]
    dystr = "%02i" % dy#[t]
    os.system("cd "+top_dir)
    case_dir  = top_dir+"Cases/"+case_name+"/"
    cdcmd = "cd "+case_dir+" ; "
    print "  case : "+case_name
    print
    #--------------------------
    # Create new case
    #--------------------------
    if newcase == True:
        if cld == "ZM" :
            #compset_opt = "-user_compset 2000_CAM5_SLND_SICE_AQUAP_SROF_SGLC_SWAV"                 # equivalent to F_2000_CAM5_AQUAPLANET
            compset_opt = "-user_compset 2000_CAM5_DLND%NULL_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV"
            #compset_opt = "-compset F_2000_CAM5"
            config_opt  = " "
        # if cld == "SP" :
        #     compset_opt = "-user_compset 2000_CAM4%SPCAM_DLND%NULL_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV"
        #     config_opt  = "-confopts -rad camrt  -crm_nz 24 -nlev 26 -phys cam4 -use_SPCAM -crm_nx 32 -crm_ny 1 -crm_dx 4000 -crm_dt 20 -SPCAM_microp_scheme sam1mom "

        cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+"_"+res+" -mach  userdefined "+config_opt
        print cmd
        os.system(cmd)
        print 


        os.system(cdcmd+"./xmlchange -file env_build.xml    -id OS                  -val Linux ")
        os.system(cdcmd+"./xmlchange -file env_build.xml    -id MPILIB              -val openmpi ")
        os.system(cdcmd+"./xmlchange -file env_build.xml    -id GMAKE               -val make ")
        os.system(cdcmd+"./xmlchange -file env_build.xml    -id COMPILER            -val gnu")
        os.system(cdcmd+"./xmlchange -file env_build.xml    -id EXEROOT             -val "+run_dir+case_name+"/bld ")
        os.system(cdcmd+"./xmlchange -file env_run.xml      -id RUNDIR              -val "+run_dir+case_name+"/run")
        os.system(cdcmd+"./xmlchange -file env_run.xml      -id DIN_LOC_ROOT        -val ~/Model/CESM/inputdata")
        os.system(cdcmd+"./xmlchange -file env_run.xml      -id CESMSCRATCHROOT     -val ~/Model/CESM/scratch")
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id MAX_TASKS_PER_NODE  -val 1")
    #--------------------------
    # Configure the case
    #--------------------------
    if config == True:
        # set run-time variables
        start_date = yrstr+"-"+mnstr+"-"+dystr
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE  -val "+start_date)
        #os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_START    -val 0000")
        #os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_END      -val 3000")
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_DATA_FILENAME -val "+data_filename)

        # set processor number
        ntask = 16
        nthrd =  1
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ATM -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_LND -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ICE -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_OCN -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_CPL -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_GLC -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ROF -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_WAV -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ATM -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_LND -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ICE -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_OCN -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_CPL -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_GLC -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ROF -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_WAV -val "+str(nthrd))
        

        # Copy the custom namelist file
        # srcnml = nmlmod_dir+"user_nl_cam.aqua" #+"."+member
        # desnml = case_dir+"user_nl_cam"
        # ifile = open(srcnml, 'r')
        # ofile = open(desnml, 'w')
        # #for line in ifile: ofile.write( line.replace("iopfile", " iopfile = '"+data_filename+"'") )
        # #for line in ifile: ofile.write( line.replace("ncdata", " ncdata = '"+data_filename+"'") )
        # for line in ifile: ofile.write( line )
        # ifile.close()
        # ofile.close()

        # We also need modified docn and dlnd namelists (for domain file paths)
        os.system("cp "+nmlmod_dir+"scylla.user_nl_cam.aqua  "+case_dir+"user_nl_cam")
        os.system("cp "+nmlmod_dir+"scylla.user_nl_docn.aqua "+case_dir+"user_nl_docn")
        os.system("cp "+nmlmod_dir+"scylla.user_nl_docn.aqua "+case_dir+"user_nl_dice")
        os.system("cp "+nmlmod_dir+"scylla.user_nl_dlnd.aqua "+case_dir+"user_nl_dlnd")
        
        # the cpl namelist has the orbital parameters
        os.system("cp "+nmlmod_dir+"scylla.user_nl_cpl.aqua  "+case_dir+"user_nl_cpl ")

        # configure the case
        os.system(cdcmd+"./cesm_setup -clean")
        os.system(cdcmd+"./cesm_setup")
    #--------------------------
    # Build the model
    #--------------------------
    if build == True:
        os.system(cdcmd+"./"+case_name+".clean_build")          # Clean previous build
        #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
        os.system(cdcmd+"./"+case_name+".build")                # Build the model
    #--------------------------
    # Run the simulation
    #--------------------------
    if runsim == True:
        runfile = case_dir+case_name+".run"
        subfile = case_dir+case_name+".submit"
        tmpfile = case_dir+case_name+".run_tmp"

        os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 

        # Uncomment the mpirun statement
        ifile = open(runfile, 'r')
        ofile = open(tmpfile, 'w')
        for line in ifile: 
            if line.startswith('#mpirun'): line = line[1:]
            ofile.write(line)
        ofile.close()
        os.system("cp "+tmpfile+" "+runfile)
        os.remove(tmpfile)

        # run the model
        cmd = cdcmd+" nohup time "+runfile+" > ~/Model/CESM/logs/"+case_name+".log &"
        print
        print cmd
        print
        os.system(cmd)

        #os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN   -val TRUE")
    #--------------------------
    #--------------------------
    del case_dir
