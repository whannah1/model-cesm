#!/bin/csh -f

# svn list https://svn-ccsm-release.cgd.ucar.edu/model_versions

# svn list --username guestuser https://svn-ccsm-release.cgd.ucar.edu/model_versions
# svn co --username guestuser https://svn-ccsm-release.cgd.ucar.edu/model_versions/cesm1_0/





echo ""
echo "  subversion data checkout..."
echo ""


 set svnrepo='https://svn-ccsm-inputdata.cgd.ucar.edu/trunk/inputdata'
 set INPUTDATA="/maloney-scratch/whannah/CESM_SCM/input_data"

   




mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart_aero/emis/
   cd $INPUTDATA/atm/cam/chem/trop_mozart_aero/emis/
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_bc_elev_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_bc_surf_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_num_a1_elev_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_num_a2_elev_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_num_a1_surf_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_num_a2_surf_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_oc_elev_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_oc_surf_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_so4_a1_elev_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_so4_a2_elev_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_so4_a1_surf_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_so4_a2_surf_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_so2_surf_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_so2_elev_2000_c090726.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/ar5_mam3_soag_1.5_surf_2000_c100217.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/emis/aerocom_mam3_dms_surf_2000_c090129.nc




mkdir -p $INPUTDATA/atm/cam/scamocphi_rrtmg_c080918.nc
mkdir -p $INPUTDATA/atm/cam/scam/iop
   cd $INPUTDATA/atm/cam/scam/iop
   svn export $svnrepo/atm/cam/scam/iop/arm0795v1.2.nc



mkdir -p $INPUTDATA/atm/cam/ozone
   #cd $INPUTDATA/atm/cam/
   #svn export $svnrepo/atm/cam/ozone
   cd $INPUTDATA/atm/cam/ozone
   svn export $svnrepo/atm/cam/ozone/ozone_1.9x2.5_L26_2000clim_c091112.nc

mkdir -p $INPUTDATA/atm/cam/rad
   cd $INPUTDATA/atm/cam/rad
   svn export $svnrepo/atm/cam/rad/abs_ems_factors_fastvx.c030508.nc



mkdir -p $INPUTDATA/atm/cam/solar
cd $INPUTDATA/atm/cam/solar
   svn export $svnrepo/atm/cam/solar/solar_ave_sc19-sc23.c090810.nc
   



mkdir -p $INPUTDATA/atm/cam/topo
   cd $INPUTDATA/atm/cam/topo
   svn export $svnrepo/atm/cam/topo/USGS-gtopo30_64x128_c050520.nc
   #svn export $svnrepo/atm/cam/topo/USGS-gtopo30_8x16_c050520.nc
   #svn export $svnrepo/atm/cam/topo/topo-from-cami_0000-09-01_8x16_L26_c030918.nc




mkdir -p $INPUTDATA/atm/cam/dst
   cd $INPUTDATA/atm/cam/dst
   svn export $svnrepo/atm/cam/dst/dst_64x128_c090203.nc
   #svn export $svnrepo/atm/cam/dst/dst_10x15_c090203.nc
   #svn export $svnrepo/atm/cam/dst/dst_8x16_c090203.nc

mkdir -p $INPUTDATA/atm/waccm
mkdir -p $INPUTDATA/atm/waccm/phot
   cd $INPUTDATA/atm/waccm/phot
   svn export $svnrepo/atm/waccm/phot/RSF_GT200nm_v3.0_c080416.nc
   svn export $svnrepo/atm/waccm/phot/temp_prs_GT200nm_jpl06_c080930.nc


mkdir -p $INPUTDATA/ocn/docn7/SSTDATA
   cd $INPUTDATA/ocn/docn7/SSTDATA
   svn export $svnrepo/ocn/docn7/SSTDATA/sst_HadOIBl_bc_1.9x2.5_clim_c061031.nc
   svn export $svnrepo/ocn/docn7/SSTDATA/sst_HadOIBl_bc_4x5_clim_c061031.nc
   #svn export $svnrepo/ocn/docn7/SSTDATA/sst_HadOIBl_bc_64x128_clim_c050526.nc


mkdir -p $INPUTDATA/atm/cam/inic/fv
mkdir -p $INPUTDATA/atm/cam/inic/gaus
   cd $INPUTDATA/atm/cam/inic/fv
   svn export $svnrepo/atm/cam/inic/fv/cami_0000-01-01_10x15_L30_c081013.nc
   #svn export $svnrepo/atm/cam/inic/fv/cami_0000-01-01_10x15_L26_c030918.nc
   cd $INPUTDATA/atm/cam/inic/gaus
   svn export $svnrepo/atm/cam/inic/gaus/cami_0000-01-01_64x128_L30_c090102.nc
   #svn export $svnrepo/atm/cam/inic/gaus/cami_0000-01-01_64x128_T42_L26_c031110.nc
   #svn export $svnrepo/atm/cam/inic/gaus/cami_0000-01-01_8x16_L26_c030228.nc
   svn export $svnrepo/atm/cam/inic/gaus/cami_0000-01-01_8x16_L30_c090102.nc




mkdir -p $INPUTDATA/atm/cam/sst
mkdir -p $INPUTDATA/atm/cam/ocnfrac
   cd $INPUTDATA/atm/cam/sst
   svn export $svnrepo/atm/cam/sst/sst_HadOIBl_bc_10x15_clim_c050526.nc
   svn export $svnrepo/atm/cam/sst/sst_HadOIBl_bc_64x128_clim_c050526.nc
   svn export $svnrepo/atm/cam/sst/sst_HadOIBl_bc_8x16_clim_c050526.nc
   cd $INPUTDATA/atm/cam/ocnfrac
   svn export $svnrepo/atm/cam/ocnfrac/domain.camocn.10x15_USGS_070807.nc
   svn export $svnrepo/atm/cam/ocnfrac/domain.camocn.64x128_USGS_070807.nc
   svn export $svnrepo/atm/cam/ocnfrac/domain.camocn.8x16_USGS_070807.nc


mkdir -p $INPUTDATA/atm/cam/chem
mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart
mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart/dvel
mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart/ub
mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart/phot
mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart_aero
mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart_aero/aero

mkdir -p $INPUTDATA/atm/cam/chem/trop_mozart_aero/oxid/
   cd $INPUTDATA/atm/cam/chem/trop_mozart_aero/oxid/
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/oxid/oxid_1.9x2.5_L26_1850-2005_c091123.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/oxid/oxid_1.9x2.5_L26_clim_list.c090805.txt
   cd $INPUTDATA/atm/cam/chem/trop_mozart_aero/aero/
   svn export $svnrepo/atm/cam/chem/trop_mozart_aero/aero/aero_1.9x2.5_L26_2000clim_c090803.nc
   cd $INPUTDATA/atm/cam/chem/trop_mozart/ub/
   svn export $svnrepo/atm/cam/chem/trop_mozart/ub/clim_p_trop.nc
   cd $INPUTDATA/atm/cam/chem/trop_mozart/phot/
   svn export $svnrepo/atm/cam/chem/trop_mozart/phot/exo_coldens.nc
   cd $INPUTDATA/atm/cam/chem/trop_mozart/dvel/
   svn export $svnrepo/atm/cam/chem/trop_mozart/dvel/regrid_vegetation.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart/dvel/season_wes.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart/dvel/clim_soilw.nc
   svn export $svnrepo/atm/cam/chem/trop_mozart/dvel/depvel_monthly.nc
   


mkdir -p $INPUTDATA/atm/cam/physprops
   cd $INPUTDATA/atm/cam/physprops
   svn export $svnrepo/atm/cam/physprops/modal_optics_3mode_c100507.nc
   svn export $svnrepo/atm/cam/physprops/dust4_rrtmg_c090521.nc
   svn export $svnrepo/atm/cam/physprops/bcpho_rrtmg_c100508.nc
   svn export $svnrepo/atm/cam/physprops/ocpho_rrtmg_c101112.nc
   svn export $svnrepo/atm/cam/physprops/ocphi_rrtmg_c100508.nc
   svn export $svnrepo/atm/cam/physprops/ssam_rrtmg_c100508.nc
   svn export $svnrepo/atm/cam/physprops/sulfate_rrtmg_c080918.nc
   svn export $svnrepo/atm/cam/physprops/water_refindex_rrtmg_c080910.nc
   svn export $svnrepo/atm/cam/physprops/iceoptics_c080917.nc
   svn export $svnrepo/atm/cam/physprops/F_nwvl200_mu20_lam50_res64_t298_c080428.nc

   #svn export $svnrepo/atm/cam/physprops/dust1_rrtmg_c090521.nc
   #svn export $svnrepo/atm/cam/physprops/bcpho_rrtmg_c090310.nc
   #svn export $svnrepo/atm/cam/physprops/ocpho_rrtmg_c100528.nc
   #svn export $svnrepo/atm/cam/physprops/ocphi_rrtmg_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/ssam_rrtmg_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/sscm_rrtmg_c080918.nc
   
   
   
   #svn export $svnrepo/atm/cam/physprops/sulfate_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/dust1_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/dust2_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/dust3_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/dust4_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/bcpho_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/bcphi_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/ocpho_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/ocphi_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/ssam_camrt_c080918.nc
   #svn export $svnrepo/atm/cam/physprops/sscm_camrt_c080918.nc


mkdir -p $INPUTDATA/lnd
mkdir -p $INPUTDATA/lnd/clm2
mkdir -p $INPUTDATA/lnd/clm2/snicardata
mkdir -p $INPUTDATA/lnd/clm2/pftdata
mkdir -p $INPUTDATA/lnd/clm2/griddata
mkdir -p $INPUTDATA/lnd/clm2/surfdata
   cd $INPUTDATA/lnd/clm2/snicardata
   svn export $svnrepo/lnd/clm2/snicardata/aerosoldep_monthly_1990s_mean_64x128_c080410.nc
   svn export $svnrepo/lnd/clm2/snicardata/snicar_drdt_bst_fit_60_c070416.nc
   svn export $svnrepo/lnd/clm2/snicardata/snicar_optics_5bnd_c090915.nc
   cd $INPUTDATA/lnd/clm2/pftdata
   svn export $svnrepo/lnd/clm2/pftdata/pft-physiology.c081222
   cd $INPUTDATA/lnd/clm2/griddata
   svn export $svnrepo/lnd/clm2/griddata/fracdata_64x128_USGS_070110.nc
   svn export $svnrepo/lnd/clm2/griddata/griddata_64x128_060829.nc
   cd $INPUTDATA/lnd/clm2/surfdata
   svn export $svnrepo/lnd/clm2/surfdata/surfdata_64x128_simyr2000_c090928.nc
   svn export $svnrepo/lnd/clm2/surfdata/surfdata_64x128_urb3den_simyr2000_c090420.nc







echo ""
echo "  done."
echo ""
