#!/usr/bin/env python
#==========================================================================================
#   This script runs an ensemble of Single-Colum CAM (SCAM) simulations
#
#    Mar, 2015  Walter Hannah       University of Miami
#
# NOTES:
#==========================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#==========================================================================================
#==========================================================================================
newcase,config,build,copyinit,runsim = False,False,False,False,False

#newcase  = True
#config   = True
#build    = True
##copyinit = True
runsim   = True

case_num = "05"

res = "0.9x1.25_gx1v6"
#res = "0.9x1.25_0.9x1.25"
#res = "f09_f09"
cld = "ZM"     #  ZM / SP
exp = "CTL"    # CTL / EXP
mod = "LTEST"    # CPL / SST / C02

cflag = "TRUE"          # Don't forget to set this!!!! 
ndays = 20#4*365
resub = 0#4

case_name = "CESM_"+cld+"_"+exp+"-"+mod+"_"+res+"_"+case_num

#===================================================================================
#===================================================================================
top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/"

run_dir = "/glade/scratch/whannah/"
src_dir = "~/Model/CESM/CESM_SRC/cesm1_1_2_spcam/"
#===================================================================================
#===================================================================================
os.system("cd "+top_dir)
case_dir  = top_dir+"Cases/"+case_name+"/"
cdcmd = "cd "+case_dir+" ; "
print
print "  case : "+case_name
print
#--------------------------
# Create new case
#--------------------------
if newcase == True:
    compfile = "/glade/u/home/whannah/Model/CESM/mod_compset/B_CUSTOM.xml"
    if cld == "ZM" : compset_opt = "-compset B_1850_ZMCAM4_CN  -compset_file "+compfile
    if cld == "SP" : compset_opt = "-compset B_1850_SPCAM4_1mom_CN -compset_file "+compfile
    
    #if mod == "LTEST" : compset_opt = "-compset F_1850_LTEST  -compset_file "+compfile
    if mod == "LTEST" : compset_opt = "-compset B_1850_ZMCAM4_CN_TEST  -compset_file "+compfile
    
    if mod == "FTEST" and case_num=="00" : compset_opt = "-compset F_1850 "
    if mod == "FTEST" and case_num=="01" : compset_opt = "-compset F_1850_L26TEST -compset_file "+compfile
    if mod == "FTEST" and case_num=="02" : compset_opt = "-compset F_1850_L28TEST -compset_file "+compfile
    if mod == "FTEST" and case_num=="03" : compset_opt = "-compset F_1850_L28TEST -compset_file "+compfile
    
    #compset_opt = "-compset F_2000"
    
    newcase_cmd = src_dir+"scripts/create_newcase"
    cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+" -mach  yellowstone "
    print cmd
    os.system(cmd)
#--------------------------
# Configure the case
#--------------------------
if config == True:
    # set run-time variables
    if exp=="CTL" : start_date = "1850-01-01"
    #if exp=="CTL" and case_num=="00" : start_date = "1850-01-01"
    #if exp=="CTL" and case_num=="01" : start_date = "1950-01-01"
    if exp=="EXP" and case_num=="00" : start_date = "2100-01-01"
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE  -val "+start_date)
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CLM_CO2_TYPE   -val diagnostic")
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CCSM_BGC       -val CO2A")
    #------------------------------------------------
    # Copy the custom namelist file
    #------------------------------------------------
    #if exp=="CTL" : os.system("cp "+nmlmod_dir+"user_nl_cam.CTL."+case_num+"  "+case_dir+"user_nl_cam ")
    #if exp=="EXP" : os.system("cp "+nmlmod_dir+"user_nl_cam.EXP."+case_num+"  "+case_dir+"user_nl_cam ")
    if mod=="LTEST" : os.system("cp "+nmlmod_dir+"user_nl_cam.LTEST."+case_num+"  "+case_dir+"user_nl_cam ")
    if mod=="FTEST" and case_num=="02" : os.system("cp "+nmlmod_dir+"user_nl_cam.FTEST."+case_num+"  "+case_dir+"user_nl_cam ")
    if mod=="FTEST" and case_num=="03" : os.system("cp "+nmlmod_dir+"user_nl_cam.FTEST."+case_num+"  "+case_dir+"user_nl_cam ")
    if mod=="FTEST" and case_num=="04" : os.system("cp "+nmlmod_dir+"user_nl_cam.FTEST."+case_num+"  "+case_dir+"user_nl_cam ")
    #os.system("cp "+nmlmod_dir+"user_nl_cam.LTEST.00  "+case_dir+"user_nl_cam ")
    #os.system("cp "+nmlmod_dir+"user_nl_pop2.CTL  "+case_dir+"user_nl_pop2 ")
    #------------------------------------------------
    # copy any modified source code (preserve permissions)
    #------------------------------------------------
    #os.system("cp -rp "+srcmod_dir+"*  "+case_dir+"SourceMods/ ")
    #os.system("touch "+case_dir+"Sourcemods/src.pop2/gx1v6_tavg_contents")
    #------------------------------------------------
    # configure the case
    #------------------------------------------------
    os.system(cdcmd+"./cesm_setup -clean")
    os.system(cdcmd+"./cesm_setup")
    
    #if mod == "CPL" and case_num=="00" :
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_TYPE    -val hybrid")
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id GET_REFCASE -val TRUE")
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFCASE -val b40.1850.track1.1deg.006")
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFDATE -val 0863-01-01")
#--------------------------
# Build the model
#--------------------------
if build == True:
    os.system(cdcmd+"./"+case_name+".clean_build")                  # Clean previous build
    #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
    os.system(cdcmd+"./"+case_name+".build")                        # Build the model
#--------------------------
# Copy init data
#--------------------------
if copyinit == True:
    print 
    if exp=="CTL" and case_num=="01" : branch_data = "/glade/p/work/whannah/CESM_ZM_CTL-CPL_0.9x1.25_gx1v6_00/1950-01-01-00000/* "
    #if exp=="EXP" and case_num=="01" : branch_data = "/glade/p/work/whannah/CESM_ZM_EXP-CPL_0.9x1.25_gx1v6_00/2200-01-01-00000/* "
    os.system("cp "+branch_data+" "+run_dir+case_name+"/run")
    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_TYPE    -val hybrid")
    os.system(cdcmd+"./xmlchange -file env_run.xml     -id GET_REFCASE -val FALSE")
    if exp=="CTL" and case_num=="01" :
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFCASE -val CESM_ZM_CTL-CPL_0.9x1.25_gx1v6_00")
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFDATE -val "+start_date)
    if exp=="EXP" and case_num=="01" :
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFCASE -val CESM_ZM_EXP-CPL_0.9x1.25_gx1v6_00")
        os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFDATE -val "+start_date)
#--------------------------
# Run the simulation
#--------------------------
if runsim == True:
    runfile = case_dir+case_name+".run"
    subfile = case_dir+case_name+".submit"
    
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RESUBMIT     -val "+str(resub))
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
    #os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val FALSE")
    
    os.system("sed -i '/#BSUB -R \"select*/d'  "+runfile)
    os.system("sed -i '/#BSUB -W/ c\#BSUB -W 1:00'  "+runfile)
    
    os.system("sed -i '/#BSUB -P/ c\#BSUB -P P35081334' "+runfile)
    
    os.system(cdcmd+subfile)
    
    
#--------------------------
#--------------------------
del case_dir

