#!/usr/bin/env python
#===================================================================================
#   This script transfers output CESM data for a specified case to scylla.meas.ncsu.edu
#    June, 2015  Walter Hannah       North Carolina State University
#===================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#===================================================================================
#===================================================================================
res = "1.9x2.5"
cld = "ZM"      #  ZM / SP

case_num = "08"

#exp = ["604","644"]
#exp = ["004","404","444"]
exp = ["444"]

case_name = "AQUA_"+cld+"_"+case_num+"_"+res+"_"
#===================================================================================
#===================================================================================
top_dir     = home+"/Model/CESM/"
os.system("cd "+top_dir)
for c in range(0,len(exp)) :
    case = case_name + exp[c]
    case_dir  = top_dir+"Cases/"+case+"/"
    cdcmd = "cd "+case_dir+" ; "
    os.system(cdcmd+"bsub < ./"+case+".l_archive")
#===================================================================================
#===================================================================================
