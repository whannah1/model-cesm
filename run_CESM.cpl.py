#!/usr/bin/env python
#==========================================================================================
#   This script runs simulations of the fully coupled CESM version 1.1.1 - slightly modified (1.1.2)
#
#    Mar, 2015  Walter Hannah       NorthCarolina State University 
#
# NOTES:    
#       The SST case mod is meant to rerun EXP with 1xCO2 and SST from EXP
#       The CO2 case mod is meant to rerun EXP with 4xCO2 and SST from CTL
#==========================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#==========================================================================================
#==========================================================================================
newcase,config,build,copyinit,runsim = False,False,False,False,False

#newcase  = True
#config   = True
#build    = True
#copyinit = True
runsim   = True

case_num = "02"

res = "0.9x1.25_gx1v6"
cld = "ZM"     #  ZM / SP
exp = "EXP"    # CTL / EXP
mod = "CPL"    # CPL / SST / C02

cflag = "TRUE"                      # Don't forget to set this!!!! 

if cld=="ZM" :
    ndays = 365#4*365
    resub = 8#4
if cld=="SP" :
    ndays = 73
    resub = 1*5-1 # [#years]*5-1

case_name = "CESM_"+cld+"_"+exp+"-"+mod+"_"+res+"_"+case_num

#===================================================================================
#===================================================================================
top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/"

run_dir = "/glade/scratch/whannah/"
src_dir = "~/Model/CESM/CESM_SRC/cesm1_1_2_spcam/"

if exp=="CTL" and mod=="CPL" and case_num=="01" :
    ref_case = "CESM_ZM_CTL-CPL_0.9x1.25_gx1v6_00"
    ref_date = "1870-01-01"
# case #02 is for testing when convective momentum transport (CMT) is disabled for ZM-CESM
if exp=="CTL" and mod=="CPL" and case_num=="02" :
    ref_case = "CESM_ZM_CTL-CPL_0.9x1.25_gx1v6_00"
    ref_date = "1870-01-01"
if exp=="EXP" and mod=="CPL" and case_num=="01" :
    ref_case = "CESM_ZM_EXP-CPL_0.9x1.25_gx1v6_00"
    ref_date = "2340-01-01"
if exp=="EXP" and mod=="CPL" and case_num=="02" :
    ref_case = "CESM_ZM_EXP-CPL_0.9x1.25_gx1v6_00"
    ref_date = "2340-01-01"
#===================================================================================
#===================================================================================
os.system("cd "+top_dir)
case_dir  = top_dir+"Cases/"+case_name+"/"
cdcmd = "cd "+case_dir+" ; "
print
print "  case : "+case_name
print
#--------------------------
# Create new case
#--------------------------
if newcase == True:
    compfile = "/glade/u/home/whannah/Model/CESM/mod_compset/B_CUSTOM.xml"
    if cld=="ZM" and mod=="CPL" : compset_opt = "-compset B_1850_ZMCAM4_CN  -compset_file "+compfile
    if cld=="SP" and mod=="CPL" : compset_opt = "-compset B_1850_SPCAM4_1mom_CN -compset_file "+compfile
    
    if cld=="ZM" and mod=="SST" : compset_opt = "-compset F_1850_ZMCAM4_CN  -compset_file "+compfile
    if cld=="SP" and mod=="SST" : compset_opt = "-compset F_1850_SPCAM4_1mom_CN -compset_file "+compfile
    
    newcase_cmd = src_dir+"scripts/create_newcase"
    cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+" -mach  yellowstone "
    print cmd
    os.system(cmd)
#--------------------------
# Configure the case
#--------------------------
if config == True:
    # set run-time variables
    if exp=="CTL" : start_date = "1850-01-01"
    if exp=="EXP" : start_date = "2100-01-01"
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE  -val "+start_date)
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CLM_CO2_TYPE   -val diagnostic")
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CCSM_BGC       -val CO2A")
    #------------------------------------------------
    # Copy the custom namelist file
    #------------------------------------------------
    nml_file = ""
    if mod=="CPL" : nml_file = nmlmod_dir+"user_nl_cam."+exp+"."+case_num
    
    if not os.path.isfile(nml_file) :
        print "ERROR: CAM namelist file does not exist! ("+nml_file+")"
        exit()
    
    os.system("cp "+nml_file+"  "+case_dir+"user_nl_cam ")
    
    #if exp=="CTL" and mod=="CPL" : 
    #if exp=="EXP" and mod=="CPL" : os.system("cp "+nmlmod_dir+"user_nl_cam.EXP."+case_num+"  "+case_dir+"user_nl_cam ")
    
    #if exp=="EXP" and mod=="SST" : os.system("cp "+nmlmod_dir+"user_nl_cam.EXP.SST."+case_num+"  "+case_dir+"user_nl_cam ")
    #if exp=="EXP" and mod=="CO2" : os.system("cp "+nmlmod_dir+"user_nl_cam.EXP.CO2."+case_num+"  "+case_dir+"user_nl_cam ")
    
    if mod=="CPL" : os.system("cp "+nmlmod_dir+"user_nl_pop2.CTL  "+case_dir+"user_nl_pop2 ")
    #------------------------------------------------
    # copy any modified source code (preserve permissions)
    #------------------------------------------------
    if cld=="ZM" and mod=="CPL" and case_num=="02" :
        os.system("cp -rp "+srcmod_dir+"zm_conv_intr.no-CMT.F90  "+case_dir+"SourceMods/src.cam/zm_conv_intr.F90 ")
    #os.system("touch "+case_dir+"Sourcemods/src.pop2/gx1v6_tavg_contents")
    #------------------------------------------------
    # configure the case
    #------------------------------------------------
    os.system(cdcmd+"./cesm_setup -clean")
    os.system(cdcmd+"./cesm_setup")
    
    #if mod == "CPL" and case_num=="00" :
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_TYPE    -val hybrid")
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id GET_REFCASE -val TRUE")
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFCASE -val b40.1850.track1.1deg.006")
    #    os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFDATE -val 0863-01-01")
    #if exp=="CTL" and mod=="CPL" and case_num=="01" :
    if mod == "CPL" and case_num=="01" :
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_TYPE    -val hybrid")
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id GET_REFCASE -val FALSE")
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFCASE -val "+ref_case)
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFDATE -val "+ref_date)
    if mod == "CPL" and case_num=="02" :
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_TYPE    -val hybrid")
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id GET_REFCASE -val FALSE")
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFCASE -val "+ref_case)
        os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_REFDATE -val "+ref_date)
#--------------------------
# Build the model
#--------------------------
if build == True:
    os.system(cdcmd+"./"+case_name+".clean_build")                  # Clean previous build
    #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
    os.system(cdcmd+"./"+case_name+".build")                        # Build the model
#--------------------------
# Copy init data
#--------------------------
if copyinit == True:
    print 
    if case_num=="01" : branch_data = "/glade/p/work/whannah/"+ref_case+"/"+ref_date+"-00000/* "
    if case_num=="02" : branch_data = "/glade/p/work/whannah/"+ref_case+"/"+ref_date+"-00000/* "
    os.system("cp "+branch_data+" "+run_dir+case_name+"/run")
#--------------------------
# Run the simulation
#--------------------------
if runsim == True:
    runfile = case_dir+case_name+".run"
    subfile = case_dir+case_name+".submit"
    
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RESUBMIT     -val "+str(resub))
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
    #os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val FALSE")
    
    os.system("sed -i '/#BSUB -R \"select*/d'  "+runfile)
    #os.system("sed -i '/#BSUB -W/ c\#BSUB -W 1:00'  "+runfile)
    
    os.system(cdcmd+subfile)
    
    
#--------------------------
#--------------------------
del case_dir

