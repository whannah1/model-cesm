#!/usr/bin/env python
#==========================================================================================
#   This script runs atmosphere only simulations of CESM version 1.1.2 (slightly modified to fix SP bugs)
#
#    Mar, 2016  Walter Hannah       NorthCarolina State University 
#
# NOTES:    
#       ?
#==========================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#==========================================================================================
#==========================================================================================
newcase,config,build,copyinit,runsim = False,False,False,False,False

# newcase  = True
# config   = True
build    = True
# runsim   = True

case_num = "00"

res = "1.9x2.5"    #   0.9x1.25  /  1.9x2.5
cld = "SP"     #  ZM / SP
exp = "CTL"    # CTL / EXP

cflag = "FALSE"                      # Don't forget to set this!!!! 

if cld=="ZM" :
    ndays = 365 #4*365
    # resub = 0 #8 #4
if cld=="SP" :
    ndays = 1#73
    # resub = 0#1*5-1 # [#years]*5-1

case_name = "CAM_"+cld+"_"+exp+"_"+res+"_"+case_num

#===================================================================================
#===================================================================================
top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/"

# run_dir = "/glade/scratch/whannah/"
run_dir = home+"/Model/CESM/scratch/"
src_dir = "~/Model/CESM/CESM_SRC/cesm1_1_2_spcam/"

#===================================================================================
#===================================================================================
os.system("cd "+top_dir)
case_dir  = top_dir+"Cases/"+case_name+"/"
cdcmd = "cd "+case_dir+" ; "
print
print "  case : "+case_name
print
#--------------------------
# Create new case
#--------------------------
if newcase == True:
    # if cld=="ZM" : 
    if case_num == "00" : compset_opt = "-compset F_1850 "
    if case_num == "01" : compset_opt = "-compset F_1850 "
    if case_num == "02" : compset_opt = "-compset F_2000 "
    #if case_num == "03" : compset_opt = "-compset F_1850 "

    config_opt  = " "
    
    newcase_cmd = src_dir+"scripts/create_newcase"
    # cmd = newcase_cmd+" -case "+case_dir+" "+compset_opt+" -res "+res+"_"+res+" -mach  yellowstone "
    cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+"_"+res+" -mach  userdefined "+config_opt
    print cmd
    os.system(cmd)
    print 

    os.system(cdcmd+"./xmlchange -file env_build.xml    -id OS                  -val Linux ")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id MPILIB              -val openmpi ")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id GMAKE               -val make ")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id COMPILER            -val gnu")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id EXEROOT             -val "+run_dir+case_name+"/bld ")
    os.system(cdcmd+"./xmlchange -file env_run.xml      -id RUNDIR              -val "+run_dir+case_name+"/run")
    os.system(cdcmd+"./xmlchange -file env_run.xml      -id DIN_LOC_ROOT        -val ~/Model/CESM/inputdata")
    # os.system(cdcmd+"./xmlchange -file env_run.xml      -id CESMSCRATCHROOT     -val ~/Model/CESM/scratch")
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id MAX_TASKS_PER_NODE  -val 1")
#--------------------------
# Configure the case
#--------------------------
if config == True:
    # set run-time variables
    if exp=="CTL" : 
        if case_num == "00" : start_date = "1850-01-01"
        if case_num == "01" : start_date = "1850-01-01"
        if case_num == "02" : start_date = "2000-01-01"
    if exp=="EXP" : start_date = "2100-01-01"
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE  -val "+start_date)
    # os.system(cdcmd+"./xmlchange -file env_run.xml   -id CLM_CO2_TYPE   -val diagnostic")
    # os.system(cdcmd+"./xmlchange -file env_run.xml   -id CCSM_BGC       -val CO2A")
    #------------------------------------------------
    # set processor number
    #------------------------------------------------
    ntask = 16
    nthrd =  1
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ATM -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_LND -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ICE -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_OCN -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_CPL -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_GLC -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ROF -val "+str(ntask))
    # os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_WAV -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ATM -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_LND -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ICE -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_OCN -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_CPL -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_GLC -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ROF -val "+str(nthrd))
    # os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_WAV -val "+str(nthrd))
    #------------------------------------------------
    # Copy the custom namelist file
    #------------------------------------------------
    nml_file = ""
    nml_file = nmlmod_dir+"user_nl_cam.F."+exp+"."+case_num
    
    if not os.path.isfile(nml_file) :
        print "ERROR: CAM namelist file does not exist! ("+nml_file+")"
        exit()
    
    os.system("cp "+nml_file+"  "+case_dir+"user_nl_cam ")
    #------------------------------------------------
    # copy any modified source code (preserve permissions)
    #------------------------------------------------
    if cld=="ZM" and case_num=="01" :
        os.system("cp -rp "+srcmod_dir+"zm_conv_intr.no-CMT.F90  "+case_dir+"SourceMods/src.cam/zm_conv_intr.F90 ")
    if cld=="SP" :
        os.system("cp -rp ~/Model/CESM/SP_mods/*  "+case_dir+"SourceMods/src.cam/ ")
    #------------------------------------------------
    # configure the case
    #------------------------------------------------
    os.system(cdcmd+"./cesm_setup -clean")
    os.system(cdcmd+"./cesm_setup")
    
    #os.system(cdcmd+"./xmlchange -file env_run.xml     -id RUN_TYPE    -val startup")
#--------------------------
# Build the model
#--------------------------
if build == True:
    os.system(cdcmd+"./"+case_name+".clean_build")                  # Clean previous build
    #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
    os.system(cdcmd+"./"+case_name+".build")                        # Build the model
#--------------------------
# Copy init data
#--------------------------
# if copyinit == True:
#     print 
#     if case_num=="01" : branch_data = "/glade/p/work/whannah/"+ref_case+"/"+ref_date+"-00000/* "
#     if case_num=="02" : branch_data = "/glade/p/work/whannah/"+ref_case+"/"+ref_date+"-00000/* "
#     os.system("cp "+branch_data+" "+run_dir+case_name+"/run")
#--------------------------
# Run the simulation
#--------------------------
# if runsim == True:
#     runfile = case_dir+case_name+".run"
#     subfile = case_dir+case_name+".submit"
    
#     os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 
#     os.system(cdcmd+"./xmlchange -file env_run.xml   -id RESUBMIT     -val "+str(resub))
#     os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
#     #os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val FALSE")
    
#     os.system("sed -i '/#BSUB -R \"select*/d'  "+runfile)
#     #os.system("sed -i '/#BSUB -W/ c\#BSUB -W 1:00'  "+runfile)
    
#     os.system(cdcmd+subfile)

if runsim == True:
    runfile = case_dir+case_name+".run"
    subfile = case_dir+case_name+".submit"
    tmpfile = case_dir+case_name+".run_tmp"

    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 

    # Uncomment the mpirun statement
    ifile = open(runfile, 'r')
    ofile = open(tmpfile, 'w')
    for line in ifile: 
        if line.startswith('#mpirun'): line = line[1:]
        ofile.write(line)
    ofile.close()
    os.system("cp "+tmpfile+" "+runfile)
    os.remove(tmpfile)

    # run the model
    cmd = cdcmd+" nohup time "+runfile+" > ~/Model/CESM/logs/"+case_name+".log &"
    print
    print cmd
    print
    os.system(cmd)
    
    
#--------------------------
#--------------------------
del case_dir

