; This script creates a stripped down version of an IOP dataset
; and adds specified perturbations to chosen variables starting
; at a given time.
; (for use in the NCAR SCAM)

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"



begin

	dir = "/Users/whannah/CESM_SCM/IOP_data/"

  	;ofile = "ptest_IOP.nc"
	
	ifile = "toga_coare.nc"
	
	
	
	
  infile = addfile(dir+ifile,"r")

  
        ;tsz = dimsizes(infile->tsec)
        ;tsz = 1440
        tsz = 5000

  print("  tsz = "+tsz)
  
  
;----------------------------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------------------------
  
  vars_to_skip = (/"tsec","Day","Month","Year"/)
  ;vars_to_null = (/""/)
  ;vars_to_null = (/"omega","divT","divq","divs","vertdivT","vertdivq","vertdivs","DT_dt","Dq_dt","ds_dt","Tg","Tsair","Q1","Q2","usrf","vsrf","colH20Adv","dcolH2Odt","dcolDSEAdt","dcolDSEdt","u","v"/)


  vars_to_null = (/""/) ;(/"omega","Tg","Q1","Q2","u","v"/)

  
  infile = addfile(dir+ifile,"r")
  var = getfilevarnames(infile)
  
  lev = infile->lev
  lat = infile->lat
  lon = infile->lon
  levsz = dimsizes(lev)

  fac = 1/(60.*60.*24.)
  ;plev = (/9,12,17,21,25,29,31,34/)
  plev = (/9,12,17,21,25/)
  num_p = dimsizes(plev)
  pvar = (/"divT"	,"divq"/)
  pval = (/-2.*fac	,2./1000.*fac	/)
  dp = 7500
  Ps = avg(infile->Ps)
  pert = lev*0.

  pert_tstart = 8-1
  
  prec_threshold1 = 20 / (60.*60.*24.*1000.)		; mm/day > m/s
  prec_threshold2 = 500 / (60.*60.*24.*1000.)		; mm/day > m/s
  
  vals = ind( (infile->Prec_diag(:,0,0).ge.prec_threshold1).and.(infile->Prec_diag(:,0,0).le.prec_threshold2) )
;=======================================================================
;=======================================================================
do p = 0,num_p

  ofile = dir+"ptest/ptest_IOP_"+sprinti("%2.2i",p)+".nc"
  if isfilepresent(ofile) then
    system("rm "+ofile)
  end if
  outfile = addfile(ofile,"c")

  pert = 0.
  if p.ne.0 then  
    pert(plev(p-1)-2:plev(p-1)+2) = (/0.25,0.5,1.,0.5,0.25/)
  end if
  pvar_cnt = 0

  
  do v = 0,dimsizes(var)-1
    ;------------------------------------------------------
    ;------------------------------------------------------
    if p.eq.0 then
      print("  "+var(v))
    end if
    if any(var(v).eq.vars_to_null) then
      print("	= NULL")
      if dimsizes(dimsizes(infile->$var(v)$)).eq.3 then
        print("!!!!!!!!!!!!!!!!!!!!")      
        tmp_out = new((/tsz,1,1/),float)
         tmp_out(:,0,0) = 0.0
         tmp_out!0 = "time"
         tmp_out!1 = "lat"
         tmp_out!2 = "lon"
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
         delete(tmp_out)
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.4 then      
        tmp_out = new((/tsz,dimsizes(lev),1,1/),float)
         do t = 0,tsz-1
          tmp_out(t,:,0,0) = 0.0
         end do
         tmp_out!0 = "time"
         tmp_out!1 = "lev"
         tmp_out!2 = "lat"
         tmp_out!3 = "lon"
         tmp_out&lev = lev
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
        delete(tmp_out)
      end if 
    ;------------------------------------------------------
    ;------------------------------------------------------
    else
    if .not.any(var(v).eq.vars_to_skip) then
      if dimsizes(dimsizes(infile->$var(v)$)).eq.1 then
        outfile->$var(v)$ = infile->$var(v)$
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.2 then
        tmp_out = infile->$var(v)$
        copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
         delete(tmp_out)
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.3 then
        tmp     = dim_avg_n(infile->$var(v)$(vals,  0,0),0)
        tmp_out = new((/tsz,1,1/),float)
         tmp_out(:,0,0) = tmp
         tmp_out!0 = "time"
         tmp_out!1 = "lat"
         tmp_out!2 = "lon"
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
         delete(tmp)
         delete(tmp_out)
      end if
      if dimsizes(dimsizes(infile->$var(v)$)).eq.4 then
        tmp     = dim_avg_n(infile->$var(v)$(vals,:,0,0),0)
        tmp_out = new((/tsz,dimsizes(lev),1,1/),float)
         do t = 0,tsz-1
          tmp_out(t,:,0,0) = tmp
         end do
         if any(var(v).eq.pvar) then
          tmp_out(pert_tstart:,:,:,:) = tmp_out(pert_tstart:,:,:,:)+conform(tmp_out(pert_tstart:,:,:,:),pval(ind(pvar.eq.var(v)))*pert,1)
          pvar_cnt = pvar_cnt+1
         end if
         tmp_out!0 = "time"
         tmp_out!1 = "lev"
         tmp_out!2 = "lat"
         tmp_out!3 = "lon"
         tmp_out&lev = lev
         tmp_out&lat = lat
         tmp_out&lon = lon
         copy_VarAtts(infile->$var(v)$,tmp_out)
        outfile->$var(v)$ = tmp_out
        delete(tmp)
        delete(tmp_out)
      end if
      end if
    end if
  end do
  ;------------------------------------------------------
  ;------------------------------------------------------
  do i = 0,dimsizes(vars_to_skip)-1
    ;outfile->$vars_to_skip(i)$ = infile->$vars_to_skip(i)$(0:tsz-1)
    tmp = infile->$vars_to_skip(i)$(0:1)
    dvtsdt = tmp(1)-tmp(0)
    tmp_out = new((/tsz/),typeof(tmp))
    tmp_out(0:) = tmp(0)+ispan(0,tsz-1,1)*dvtsdt
    outfile->$vars_to_skip(i)$ = tmp_out
    delete(tmp)
    delete(tmp_out)
  end do
  
end do
;=======================================================================
;=======================================================================
  
end

; TOGA lev 
;(0)	7500
;(1)	10000
;(2)	12500
;(3)	15000
;(4)	17500
;(5)	20000
;(6)	22500
;(7)	25000
;(8)	27500
;(9)	30000	*
;(10)	32500
;(11)	35000
;(12)	37500
;(13)	40000	*
;(14)	42500
;(15)	45000
;(16)	47500
;(17)	50000	*
;(18)	52500
;(19)	55000
;(20)	57500
;(21)	60000	*
;(22)	62500
;(23)	65000
;(24)	67500
;(25)	70000	*
;(26)	72500
;(27)	75000
;(28)	77500
;(29)	80000	*
;(30)	82500
;(31)	85000	*
;(32)	87500
;(33)	90000
;(34)	92500	*
;(35)	95000	
;(36)	97500	
;(37)	100000
