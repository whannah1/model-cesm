; Create a input "domian" file for the land and ocean components of CESM
; since this is for an aqua planet, there is all ocean and no land
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin 

    ;idir = "/home/whannah/Model/CESM/inputdata/share/domains/"
    idir = "~/Model/CESM/IC_NUNA/"
    odir = "~/Model/CESM/IC_NUNA/"

    if .not.isvar("lnd_xc") then lnd_xc  = 10  end if   ; land mass X center
    if .not.isvar("lnd_yc") then lnd_yc  = 20  end if   ; land mass Y center
    if .not.isvar("lnd_wd") then lnd_wd  = 50. end if   ; land mass longitude width
    if .not.isvar("lnd_hg") then lnd_hg  = 28. end if   ; land mass latitude hegiht

    if .not.isvar("lmask_value") then lmask_value = 1 end if     ; set to 1, or 0 to turn off land mass
;=============================================================================
;=============================================================================
    printline()
    ifile = idir+"domain.lnd.fv1.9x2.5_gx1v6.090206.nc"
    ofile = odir+"nuna_domain.lnd.1.9x2.5_gx1v6.v1.nc"

    if isfilepresent(ofile) then system("rm "+ofile) end if
    infile  = addfile(ifile,"r")
    outfile = addfile(ofile,"c")

; print(infile)
; printline()

    lat = infile->yc(:,0)
    lon = infile->xc(0,:)

    lx1 = lnd_xc - lnd_wd/2.
    lx2 = lnd_xc + lnd_wd/2.
    ly1 = lnd_yc - lnd_hg/2.
    ly2 = lnd_yc + lnd_hg/2.

    if any((/lx1,lx2/).lt.0) then 
        tlon1 = array_append_record(lon-360,lon,0)
        ;tlon1 = where(tlon1.gt.180,tlon1-360,tlon1)
        tlon1!0 = "lon"
        tlon1&lon = tlon1
        tlon = tlon1({lx1:lx2})
        tlon = where(tlon.lt.0,tlon+360,tlon)
        ;lnd_T&lon = tlon
    end if
;=============================================================================
; Make new LND domain file
;=============================================================================
    lfrac = infile->frac
    lfrac!0 = "lat"
    lfrac!1 = "lon"
    lfrac&lat = lat
    lfrac&lon = lon
    lfrac = 0

    lmask = infile->mask
    copy_VarCoords(lfrac,lmask)
    lmask = (/0/)

    lfrac({ly1:ly2},{tlon}) = lmask_value
    lmask({ly1:ly2},{tlon}) = lmask_value

    ofrac = where(lfrac.eq.1,0,1)
    omask = where(lmask.eq.1,0,1)

; qplot_map(lmask)
; exit

    vname = getfilevarnames(infile)
    do v = 0,dimsizes(vname)-1 outfile->$vname(v)$ = infile->$vname(v)$ end do

    outfile->frac = (/ lfrac /)
    outfile->mask = (/ lmask /)

    print("")
    print("    "+ofile)
    print("")
;=============================================================================
;=============================================================================
    printline()
    ifile = idir+"domain.ocn.1.9x2.5_gx1v6_090403.nc"
    ofile = odir+"nuna_domain.ocn.1.9x2.5_gx1v6.v1.nc"

    if isfilepresent(ofile) then system("rm "+ofile) end if
    infile  = addfile(ifile,"r")
    outfile = addfile(ofile,"c")
    
    vname = getfilevarnames(infile)
    do v = 0,dimsizes(vname)-1 outfile->$vname(v)$ = infile->$vname(v)$ end do

    outfile->frac = (/ ofrac /)
    outfile->mask = (/ omask /)
    

    print("")
    print("    "+ofile)
    print("")
;=============================================================================
;=============================================================================
printline()
end