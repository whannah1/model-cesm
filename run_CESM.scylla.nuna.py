#!/usr/bin/env python
#=============================================================================================
#   This script runs an ensemble of CAM simulations with a single idealized continent (on Yellowstone)
#
#    Jan, 2016  Walter Hannah       North Carolina State University
#=============================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#=============================================================================================
#=============================================================================================
mkinit,newcase,config,build,runsim = False,False,False,False,False

#mkinit  = True
# newcase = True
# config  = True
# build   = True
runsim  = True

member  = "00"
res     = "1.9x2.5"
cld     = "ZM"       # CLUBB / UNICON / ZM

cflag = "TRUE"
ndays = 365*4 #5*365

amp = 0
xc  = 4
yc  = 4

#===================================================================================
#===================================================================================
sst_clat = 0
wp_clat  = 0
sst_yc   = "2.0"

lxc = 10
lyc = 20
lwd = 50
lhg = 28

#===================================================================================
#===================================================================================
#case_name = "AQUA_"+cld+"_"+member+"_"+res+"_"+str(amp).zfill(2)+str(xc).zfill(2)+str(yc).zfill(2)
case_name = "NUNA_"+cld+"_"+member+"_"+res+"_"+str(amp)+str(xc)+str(yc)

sst_clat_str = str(sst_clat).zfill(2)
wp_clat_str  = str( wp_clat).zfill(2)
if sst_clat < 0 : sst_clat_str = str(sst_clat).zfill(3)
if  wp_clat < 0 :  wp_clat_str = str( wp_clat).zfill(3)

data_filename = home+"/Model/CESM/IC_NUNA/sst_nuna.v2"
data_filename = data_filename+".sst_yc_"+sst_yc
data_filename = data_filename+".wp_axy_"+str(amp).zfill(2)+"_"+str(xc).zfill(2)+"_"+str(yc).zfill(2)+""
data_filename = data_filename+".lnd_xywh_"+str(lxc).zfill(2)+"_"+str(lyc).zfill(2)+"_"+str(lwd).zfill(2)+"_"+str(lhg).zfill(2)+""
data_filename = data_filename+".wp_clat_"+wp_clat_str+".sst_clat_"+sst_clat_str
data_filename = data_filename+".nc"

#data_filename = home+"/Model/CESM/IC_NUNA/sst_nuna_ideal.v2.amp_0.0.xc_4.0.yc_4.0.nc"
#data_filename = home+"/Model/CESM/IC_AQUA/sst_aqua_ideal.v2.amp_0.0.xc_0.0.yc_4.0.nc"

# surf_filename = home+"/Model/CESM/IC_NUNA/surfdata_nuna_ideal.v2"
# surf_filename = surf_filename+".lnd_xc_"+str(lxc)+".0.lnd_yc_"+str(lyc)+".0.lnd_wd_"+str(lwd)+".0.lnd_hg_"+str(lhg)+".0"
# surf_filename = surf_filename+".nc"

top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/scylla."

run_dir = home+"/Model/CESM/scratch/"
src_dir = home+"/Model/CESM/CESM_SRC/"
if cld == "ZM"     : src_dir = src_dir + "cesm1_2_2/"
# if cld == "ZM"     : src_dir = src_dir + "cesm1_1_2_spcam/"
# if cld == "UNICON" : src_dir = src_dir + "UNICON_e13b7u1.8/"
# if cld == "CLUBB"  : src_dir = src_dir + "clubbMG2_n36_cam5_3_08/"
#===================================================================================
#===================================================================================
os.system("cd "+top_dir)
case_dir  = top_dir+"Cases/"+case_name+"/"
cdcmd = "cd "+case_dir+" ; "
print
print "  case : "+case_name
print
#--------------------------
# remake input data  ( Intialization / SST / Topo )
#--------------------------
if mkinit == True :
    input_str  = " lnd_xc="+str(lxc)+" lnd_yc="+str(lyc)+" lnd_wd="+str(lwd)+" lnd_hg="+str(lhg)+"  lmask_value=1"
    script_dir = home+"/Model/CESM/"
    os.system("ncl "+input_str+" "+script_dir+"IC_code/mk.nuna.topo.v1.ncl")
    os.system("ncl "+input_str+" "+script_dir+"IC_code/mk.nuna.domain.v1.ncl")
    os.system("ncl "+input_str+" "+script_dir+"IC_code/mk.nuna.sst.v1.ncl")
    os.system("ncl "+input_str+" "+script_dir+"IC_code/mk.nuna.cami.v1.ncl")
#--------------------------
# Create new case
#--------------------------
if newcase == True:
    if cld == "ZM" :
        # if member == "00": compset_opt = "-user_compset 2000_CAM5_DLND_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV"
        if member == "00": compset_opt = "-user_compset 2000_CAM5_CLM40_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV"
        if member == "01": compset_opt = "-user_compset 2000_CAM5_CLM40_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV"
        
        
    if cld == "SP" :
        print "SP setup needs attention!"
        exit()
    newcase_cmd = src_dir+"scripts/create_newcase"
    cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+"_"+res+" -mach  userdefined "
    print cmd
    os.system(cmd)
    
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id OS                  -val Linux ")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id MPILIB              -val openmpi ")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id GMAKE               -val make ")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id COMPILER            -val gnu")
    os.system(cdcmd+"./xmlchange -file env_build.xml    -id EXEROOT             -val "+run_dir+case_name+"/bld ")
    os.system(cdcmd+"./xmlchange -file env_run.xml      -id RUNDIR              -val "+run_dir+case_name+"/run")
    os.system(cdcmd+"./xmlchange -file env_run.xml      -id DIN_LOC_ROOT        -val ~/Model/CESM/inputdata")
    os.system(cdcmd+"./xmlchange -file env_run.xml      -id CESMSCRATCHROOT     -val ~/Model/CESM/scratch")
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id MAX_TASKS_PER_NODE  -val 1")
#--------------------------
# Configure the case
#--------------------------
if config == True:
    # set run-time variables
    start_date = "0000-01-01"
    os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_STARTDATE        -val "+start_date)
    os.system(cdcmd+"./xmlchange -file env_run.xml -id SSTICE_DATA_FILENAME -val "+data_filename)

    os.system(cdcmd+"./xmlchange -file env_run.xml -id SSTICE_GRID_FILENAME -val /home/whannah/Model/CESM/IC_NUNA/nuna_domain.ocn.1.9x2.5_gx1v6.v1.nc")

    os.system(cdcmd+"./xmlchange -file env_run.xml -id LND_DOMAIN_FILE      -val "+"nuna_domain.lnd.1.9x2.5_gx1v6.v1.nc")
    os.system(cdcmd+"./xmlchange -file env_run.xml -id LND_DOMAIN_PATH      -val "+"/home/whannah/Model/CESM/IC_NUNA/")

    # if member != "00": os.system(cdcmd+"./xmlchange -file env_run.xml -id CLM_FORCE_COLDSTART  -val "+"on")
    os.system(cdcmd+"./xmlchange -file env_run.xml -id CLM_FORCE_COLDSTART  -val "+"on")
    
    #os.system(cdcmd+"./xmlchange -file env_build.xml -id DEBUG  -val TRUE")
    
    # set processor number
    ntask = 16
    nthrd =  1
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ATM -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_LND -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ICE -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_OCN -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_CPL -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_GLC -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ROF -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_WAV -val "+str(ntask))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ATM -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_LND -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ICE -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_OCN -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_CPL -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_GLC -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ROF -val "+str(nthrd))
    os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_WAV -val "+str(nthrd))

    #------------------------------------------------
    # Copy the custom namelist file
    #------------------------------------------------
    os.system("cp "+nmlmod_dir+"user_nl_cam.nuna  "+case_dir+"user_nl_cam ")
    
    # We also need modified docn and dlnd namelists (for domain file paths)
    os.system("cp "+nmlmod_dir+"user_nl_docn.nuna "+case_dir+"user_nl_docn")
    os.system("cp "+nmlmod_dir+"user_nl_dice.nuna "+case_dir+"user_nl_dice")
    
    # if member == "00": os.system("cp "+nmlmod_dir+"user_nl_dlnd.nuna "+case_dir+"user_nl_dlnd")
    if member == "00": os.system("cp "+nmlmod_dir+"user_nl_clm.nuna "+case_dir+"user_nl_clm")
    if member == "01": os.system("cp "+nmlmod_dir+"user_nl_clm.nuna "+case_dir+"user_nl_clm")

    # the cpl namelist has modified orbital parameters (set to zero for no seasons)
    if member == "01": os.system("cp "+nmlmod_dir+"user_nl_cpl.nuna  "+case_dir+"user_nl_cpl ")
    #------------------------------------------------
    # configure the case
    #------------------------------------------------
    os.system(cdcmd+"./cesm_setup -clean")
    os.system(cdcmd+"./cesm_setup ")    
#--------------------------
# Build the model
#--------------------------
if build == True:
    os.system(cdcmd+"./"+case_name+".clean_build")          # Clean previous build
    #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
    os.system(cdcmd+"./"+case_name+".build")                # Build the model
#--------------------------
# Run the simulation
#--------------------------
if runsim == True:
    runfile = case_dir+case_name+".run"
    subfile = case_dir+case_name+".submit"
    tmpfile = case_dir+case_name+".run_tmp"

    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 

    # Uncomment the mpirun statement
    ifile = open(runfile, 'r')
    ofile = open(tmpfile, 'w')
    for line in ifile: 
        if line.startswith('#mpirun'): line = line[1:]
        ofile.write(line)
    ofile.close()
    os.system("cp "+tmpfile+" "+runfile)
    os.remove(tmpfile)

    # run the model
    cmd = cdcmd+" nohup time "+runfile+" > ~/Model/CESM/logs/"+case_name+".log &"
    print
    print cmd
    print
    os.system(cmd)

#--------------------------
#--------------------------
# del case_dir
