#!/usr/bin/env python
#===================================================================================
#   This script transfers output CESM data for a specified case to scylla.meas.ncsu.edu
#    June, 2015  Walter Hannah       North Carolina State University
#===================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#===================================================================================
#===================================================================================

case_num = "04"
res = "1.9x2.5"
cld = "ZM"      #  ZM / SP
#exp = "EXP"     # CTL / EXP
#mod = "CPL"     # CPL / SST / C02

#exp = ["004","204","304","404","604","444","644"]
#exp = ["404","444"]
exp = ["444"]

#case_name = "CESM_"+cld+"_"+exp+"-"+mod+"_"+res+"_"+case_num
case_name = "AQUA_"+cld+"_"+case_num+"_"+res+"_"

#tcmd = "scp -p "
tcmd = "rsync --ignore-existing --times "

mkdir = True

dst_server = "scylla.meas.ncsu.edu"

#===================================================================================
# set directory paths and destination server
#===================================================================================

for texp in exp : 
    tcase =case_name + texp
    
    src_dir = "/glade/scratch/whannah/archive/"+tcase+"/"
    dst_dir = "~/Data/CESM/"+tcase+"/" 

    print
    print tcase
    print
    #--------------------------------------
    # Make sure destination directory exists
    #--------------------------------------
    if mkdir :
        print "    creating destination directories..."
        os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"     '")
        os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"atm/ '")
        #os.system("ssh whannah@"+dst_server+" 'mkdir "+dst_dir+"ocn/ '")
    print
    #--------------------------------------
    # copy the data
    #--------------------------------------
    print "copying atm h0 data..."
    cmd = tcmd+src_dir+"atm/hist/*h0*.nc   "+dst_server+":"+dst_dir+"atm/ "
    print ""+cmd
    print
    os.system(cmd)
    print

    print "copying atm h1 data..."
    cmd = tcmd+src_dir+"atm/hist/*h1*.nc   "+dst_server+":"+dst_dir+"atm/ "
    print ""+cmd
    print
    os.system(cmd)
    print 

    #print "copying ocn data..."
    #cmd = tcmd+src_dir+"ocn/hist/*.h*nc    "+dst_server+":"+dst_dir+"ocn/ "
    #print ""+cmd
    #print
    #os.system(cmd)
    #print

#===================================================================================
#===================================================================================
print "done."
print
#===================================================================================
#===================================================================================
