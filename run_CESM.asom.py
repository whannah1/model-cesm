#!/usr/bin/env python
#=============================================================================================
#   This script runs an ensemble of CAM aqua slab ocean model (SOM) simulations (on Yellowstone)
#
#    Jan, 2016  Walter Hannah       North Carolina State University
#=============================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#=============================================================================================
#=============================================================================================
newcase,config,build,runsim = False,False,False,False

newcase = True
config  = True
build   = True
runsim  = True

member  = "03"
res     = "1.9x2.5"
cld     = "ZM"       # CLUBB / UNICON / ZM / SP

cflag = "FALSE"
ndays = 365*2  #5*365
resub = 0

amp = 0
xc  = 0
yc  = 4

#===================================================================================
#===================================================================================
sst_clat = 0
wp_clat  = 0
sst_yc   = "2.0"

#===================================================================================
#===================================================================================
#case_name = "AQUA_"+cld+"_"+member+"_"+res+"_"+str(amp).zfill(2)+str(xc).zfill(2)+str(yc).zfill(2)
case_name = "ASOM_"+cld+"_"+member+"_"+res+"_"+str(amp)+str(xc)+str(yc)

sst_clat_str = str(sst_clat).zfill(2)
wp_clat_str  = str( wp_clat).zfill(2)
if sst_clat < 0 : sst_clat_str = str(sst_clat).zfill(3)
if  wp_clat < 0 :  wp_clat_str = str( wp_clat).zfill(3)

#data_filename = home+"/Model/CESM/IC_AQUA/sst_aqua_ideal.v2"
data_filename = home+"/Model/CESM/IC_ASOM/sst_asom_ideal.v1"
data_filename = data_filename+".sst_yc_"+sst_yc
data_filename = data_filename+".amp_"+str(amp)+".0.xc_"+str(xc)+".0.yc_"+str(yc)+".0"
data_filename = data_filename+".wp_clat_"+wp_clat_str+".sst_clat_"+sst_clat_str
data_filename = data_filename+".nc"

#som_filename = home+"/Model/CESM/IC_ASOM/pop_frc.asom.v1.nc"
som_filename = "pop_frc.asom.v1.nc"

top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/"

run_dir = "/glade/scratch/whannah/"
src_dir = home+"/Model/CESM/CESM_SRC/"
if cld == "ZM"     : src_dir = src_dir + "cesm1_2_2/"
#if cld == "ZM"     : src_dir = src_dir + "cesm1_1_2_spcam/"
if cld == "UNICON" : src_dir = src_dir + "UNICON_e13b7u1.8/"
if cld == "CLUBB"  : src_dir = src_dir + "clubbMG2_n36_cam5_3_08/"
#===================================================================================
#===================================================================================
os.system("cd "+top_dir)
case_dir  = top_dir+"Cases/"+case_name+"/"
cdcmd = "cd "+case_dir+" ; "
print
print "  case : "+case_name
print
#--------------------------
# Create new case
#--------------------------
if newcase == True:
    if cld == "ZM" :
        # F_2000_CAM5_AQUAPLANET = 2000_CAM5_SLND_SICE_AQUAP_SROF_SGLC_SWAV
        # E_1850_CAM5            = 1850_CAM5_CLM40%SP_CICE_DOCN%SOM_RTM_SGLC_SWAV
        # custom AQUA            = 2000_CAM5_DLND%NULL_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV
        compset_opt = "-user_compset 2000_CAM5_DLND%NULL_DICE%NULL_DOCN%SOM_SROF_SGLC_SWAV"
    newcase_cmd = src_dir+"scripts/create_newcase"
    cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+"_"+res+" -mach  yellowstone "
    print cmd
    os.system(cmd)
    
    #os.system(cdcmd+"./xmlchange -file env_build.xml    -id COMPILER            -val gnu")
#--------------------------
# Configure the case
#--------------------------
if config == True:
    # set run-time variables
    start_date = "0000-01-01"
    os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_REFDATE          -val "+start_date)
    os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_STARTDATE        -val "+start_date)
    os.system(cdcmd+"./xmlchange -file env_run.xml -id SSTICE_DATA_FILENAME -val "+data_filename)
    
    os.system(cdcmd+"./xmlchange -file env_run.xml -id SSTICE_STREAM        -val CAMDATA")
    #os.system(cdcmd+"./xmlchange -file env_run.xml -id SSTICE_GRID_FILENAME -val "+data_filename)
    os.system(cdcmd+"./xmlchange -file env_run.xml -id DOCN_SOM_FILENAME    -val "+som_filename)
    
    #os.system(cdcmd+"./xmlchange -file env_build.xml -id DEBUG  -val TRUE")
    
    # set processor number (no need on Yellowstone)
    if False :
        ntask = 60
        nthrd =  1
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ATM -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_LND -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ICE -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_OCN -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_CPL -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_GLC -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ROF -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_WAV -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ATM -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_LND -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ICE -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_OCN -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_CPL -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_GLC -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ROF -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_WAV -val "+str(nthrd))

    #------------------------------------------------
    # Copy the custom namelist file
    #------------------------------------------------
    os.system("cp "+nmlmod_dir+"user_nl_cam.aqua  "+case_dir+"user_nl_cam ")

    # We also need modified docn and dlnd namelists (for domain file paths)
    os.system("cp "+nmlmod_dir+"user_nl_dlnd.asom "+case_dir+"user_nl_dlnd")
    os.system("cp "+nmlmod_dir+"user_nl_docn.asom "+case_dir+"user_nl_docn")
    #os.system("cp "+nmlmod_dir+"user_nl_dice.aqua "+case_dir+"user_nl_dice")
    os.system("cp "+nmlmod_dir+"user_nl_docn.asom "+case_dir+"user_nl_dice")
    
    # the cpl namelist has the orbital parameters
    os.system("cp "+nmlmod_dir+"user_nl_cpl.asom  "+case_dir+"user_nl_cpl ")
    
    stream1 = "/glade/u/home/whannah/Model/CESM/IC_ASOM/docn.streams.txt.som"
    #stream1 = case_dir+"/CaseDocs/docn.streams.txt "
    stream2 = case_dir+"user_docn.streams.txt"
    os.system("cp "+stream1+" "+stream2)
    
    stream2 = case_dir+"user_docn.streams.txt.som"
    os.system("cp "+stream1+" "+stream2)
    #------------------------------------------------
    # configure the case
    #------------------------------------------------
    os.system(cdcmd+"./cesm_setup -clean")
    os.system(cdcmd+"./cesm_setup ")    
#--------------------------
# Build the model
#--------------------------
if build == True:
    os.system(cdcmd+"./"+case_name+".clean_build")          # Clean previous build
    #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
    os.system(cdcmd+"./"+case_name+".build")                # Build the model
#--------------------------
# Run the simulation
#--------------------------
if runsim == True:
    runfile = case_dir+case_name+".run"
    subfile = case_dir+case_name+".submit"

    os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RESUBMIT     -val "+str(resub))
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
    
    os.system("sed -i '/#BSUB -P/ c\#BSUB -P UNCS0007' "+runfile)
    #os.system("sed -i '/#BSUB -n/ c\#BSUB -n 60'        "+runfile)
    os.system("sed -i '/#BSUB -W/ c\#BSUB -W 4:00'      "+runfile)
    
    # run the model
    os.system(cdcmd+subfile)

#--------------------------
#--------------------------
del case_dir
