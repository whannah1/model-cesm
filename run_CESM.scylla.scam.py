#!/usr/bin/env python
#========================================================================================================================
#   This script runs an ensemble of Single-Colum CAM (SCAM) simulations
#
#    Mar, 2015  Walter Hannah       University of Miami
#
# If a segmentation fault is encountered at runtime first try using 'ulimit -s 65532' to increase the stack size
#========================================================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#========================================================================================================================
#========================================================================================================================
newcase,config,build,runsim = False,False,False,False

#newcase = True
config  = True
build   = True
runsim  = True

member  = "00"
res     = "T42"
cld     = "ZM"    #  CLUBB / UNICON / ZM

ndays    = 5
nlev     = 50

#sst_case = range(295,305,5)
#sst_case = [285,290,295,300]
sst_case = [285]

top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"src_mod/"
nmlmod_dir  = top_dir+"nml/"
run_dir     = top_dir+"/scratch/"
src_dir     = top_dir+"/CESM_SRC/"

if cld == "ZM"     : src_dir = src_dir + "cesm1_2_2/"
if cld == "UNICON" : src_dir = src_dir + "UNICON_e13b7u1.8/"
if cld == "CLUBB"  : src_dir = src_dir + "clubbMG2_n36_cam5_3_08/"
#===================================================================================
#===================================================================================
#yrmndy = 2000*10000 + np.array([ 0101 ])
yr = 2000   #np.floor( yrmndy/10000 )
mn = 1      #np.floor((yrmndy - yr*10000)/100)
dy = 1      #yrmndy - yr*10000 - mn*100

num_t = 1 #len(yrmndy)  

yrstr = "%04i" % yr#[t]
mnstr = "%02i" % mn#[t]
dystr = "%02i" % dy#[t]

# create new initialization file using the NCL script
# the script interpolates from a standard cami files
#os.system("ncl nlev="+nlev+" ~/Model/CESM/IC_code//mk.cami.v2.ncl")
#===================================================================================
#===================================================================================
print
for sst in sst_case:
    os.system("cd "+top_dir)
    case_name = "SCAM_"+cld+"_L"+str(nlev)+"_"+str(sst)+"K_"+member+"_"+res
    case_dir  = top_dir+"Cases/"+case_name+"/"
    cdcmd = "cd "+case_dir+" ; "
    iop_filename = top_dir+"/IC_SCAM/scam.rce_iop."+str(sst)+"K.nc"
    #sst_filename = top_dir+"/IC_SCAM/scam.rce_sst.nc"
    sst_filename = iop_filename
    print "  case : "+case_name
    print
    #--------------------------
    # Create new case
    #--------------------------
    if newcase == True:
        compset_opt = "-user_compset 2000_CAM5%SCAM_CLM40%SP_SICE_DOCN%DOM_RTM_SGLC_SWAV"
        #compset_opt = "-compset F_2000"

        cmd = src_dir+"scripts/create_newcase"
        #cmd = cmd+" -case "+case_dir+"  "+compset_opt+" -res  "+res+"_"+res+" -mach  userdefined "
        cmd = cmd+" -case "+case_dir+"  "+compset_opt+" -res  "+res+"_"+res+" -mach  userdefined "
        print cmd
        os.system(cmd)
        print 

        os.system(cdcmd+"./xmlchange -file env_build.xml    -id OS                  -val Linux ")
        #os.system(cdcmd+"./xmlchange -file env_build.xml    -id MPILIB              -val openmpi ")
        os.system(cdcmd+"./xmlchange -file env_build.xml    -id MPILIB              -val mpi-serial ")
        #os.system(cdcmd+"./xmlchange -file env_build.xml    -id GMAKE               -val make ")       ; needed for Darwin
        os.system(cdcmd+"./xmlchange -file env_build.xml    -id COMPILER            -val gnu")
        os.system(cdcmd+"./xmlchange -file env_build.xml    -id EXEROOT             -val "+run_dir+case_name+"/bld ")
        os.system(cdcmd+"./xmlchange -file env_run.xml      -id RUNDIR              -val "+run_dir+case_name+"/run")
        os.system(cdcmd+"./xmlchange -file env_run.xml      -id DIN_LOC_ROOT        -val ~/Model/CESM/inputdata")
        os.system(cdcmd+"./xmlchange -file env_run.xml      -id CESMSCRATCHROOT     -val ~/Model/CESM/scratch")
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id MAX_TASKS_PER_NODE  -val 1")
    #--------------------------
    # Configure the case
    #--------------------------
    if config == True:
        # set run-time variables
        start_date = yrstr+"-"+mnstr+"-"+dystr
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id RUN_STARTDATE        -val "+start_date)
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_START    -val 2000")
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_YEAR_END      -val 2001")
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id PTS_MODE             -val TRUE ")
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id PTS_LAT              -val 0 ")
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id PTS_LON              -val 155 ")
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id SSTICE_DATA_FILENAME -val "+sst_filename)
        
        # set the vertical levels
        os.system(cdcmd+"./xmlchange -file env_build.xml -id CAM_CONFIG_OPTS      -val ' -nlev "+str(nlev)+" ' ")

        # Specify the domain files if needed
        #os.system(cdcmd+"./xmlchange -file env_run.xml -id ICE_DOMAIN_FILE  -val domain.camocn.64x128_USGS_070807.nc")
        #os.system(cdcmd+"./xmlchange -file env_run.xml -id ICE_DOMAIN_FILE  -val domain.ocn.1x1.111007.nc")
        #os.system(cdcmd+"./xmlchange -file env_run.xml -id ICE_DOMAIN_FILE  -val domain.ocn.48x96_gx3v7_100114.nc")
        #os.system(cdcmd+"./xmlchange -file env_run.xml -id ICE_DOMAIN_FILE  -val domain.ocn.gx1v6.090206.nc")
        #os.system(cdcmd+"./xmlchange -file env_run.xml -id ICE_DOMAIN_FILE  -val ")

        # Copy the custom namelist file
        srcnml = nmlmod_dir+"user_nl_cam.scam" #+"."+member
        desnml = case_dir+"user_nl_cam"
        ifile = open(srcnml, 'r')
        ofile = open(desnml, 'w')
        for line in ifile: ofile.write( line.replace("iopfile", " iopfile = '"+iop_filename+"'") )
        ifile.close()
        ofile.close()

        # configure the case
        os.system(cdcmd+"./cesm_setup -clean")
        os.system(cdcmd+"./cesm_setup")
    #--------------------------
    # Build the model
    #--------------------------
    if build == True:
        #os.system(cdcmd+"./"+case_name+".clean_build")          # Clean previous build
        #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
        os.system(cdcmd+"./"+case_name+".build")                # Build the model
    #--------------------------
    # Run the simulation
    #--------------------------
    if runsim == True:
        os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN   -val FALSE")

        runfile = case_dir+case_name+".run"
        subfile = case_dir+case_name+".submit"
        tmpfile = case_dir+case_name+".run_tmp"
        #print tmpfile 

        os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N         -val "+str(ndays)) 

        # Uncomment the mpirun statement
        ifile = open(runfile, 'r')
        ofile = open(tmpfile, 'w')
        for line in ifile: 
            if line.startswith('#mpirun'): line = line[1:]
            ofile.write(line)
        ofile.close()
        os.system("cp "+tmpfile+" "+runfile)
        os.remove(tmpfile)

        # run the model
        os.system(cdcmd+runfile)

        # set the model to continue, instead of restart
        #os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN   -val TRUE")
    #--------------------------
    #--------------------------
    del case_dir
