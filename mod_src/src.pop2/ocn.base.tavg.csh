#!/bin/csh -f

cat >! $CASEBUILD/pop2conf/base.tavg.nml << EOF
tavg_freq_opt           = 'once'
tavg_freq               = 1
tavg_stream_filestrings = 'once'
tavg_file_freq_opt      = 'once'
tavg_file_freq          = 1
tavg_start_opt          = 'nstep'
tavg_start              = 0
tavg_fmt_in             = 'nc'
tavg_fmt_out            = 'nc'
ltavg_has_offset_date   = .false.
tavg_offset_years       = 1
tavg_offset_months      = 1
tavg_offset_days        = 2
ltavg_one_time_header   = .false.
EOF
