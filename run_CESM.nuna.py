#!/usr/bin/env python
#=============================================================================================
#   This script runs an ensemble of CAM simulations with a single idealized continent (on Yellowstone)
#
#    Jan, 2016  Walter Hannah       North Carolina State University
#=============================================================================================
#import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#=============================================================================================
#=============================================================================================
newcase,config,build,runsim = False,False,False,False

newcase = True
config  = True
build   = True
#runsim  = True

member  = "00"
res     = "1.9x2.5"
cld     = "ZM"       # ZM / SP/ CLUBB / UNICON 

cflag = "TRUE"
ndays = 5 #*365
resub = 0

amp = 0
xc  = 4
yc  = 4

#===================================================================================
#===================================================================================
sst_clat = 0
wp_clat  = 0
sst_yc   = "2.0"

lxc = 10
lyc = 20
lwd = 50
lhg = 28

if member == "01" : 
    sst_clat = -5
    wp_clat  = -5
    
if member == "02" : 
    sst_clat = -10
    wp_clat  = -10

if member == "03" : wp_clat = -5
if member == "04" : wp_clat = -10

if member == "05" : sst_yc   = "1.5"
if member == "06" : sst_yc   = "1.0"

if member == "07" : 
    sst_clat = -5
    wp_clat  = -5
    sst_yc   = "1.5"
    
if member == "08" : 
    sst_clat = -5
    wp_clat  = -5
    sst_yc   = "1.0"
#===================================================================================
#===================================================================================
#case_name = "NUNA_"+cld+"_"+member+"_"+res+"_"+str(amp).zfill(2)+str(xc).zfill(2)+str(yc).zfill(2)
case_name = "NUNA_"+cld+"_"+member+"_"+res+"_"+str(amp)+str(xc)+str(yc)

sst_clat_str = str(sst_clat).zfill(2)
wp_clat_str  = str( wp_clat).zfill(2)
if sst_clat < 0 : sst_clat_str = str(sst_clat).zfill(3)
if  wp_clat < 0 :  wp_clat_str = str( wp_clat).zfill(3)

data_filename = home+"/Model/CESM/IC_NUNA/sst_nuna_ideal.v2"
data_filename = data_filename+".sst_yc_"+sst_yc
data_filename = data_filename+".wp_amp_"+str(amp)+".0.wp_xc_"+str(xc)+".0.wp_yc_"+str(yc)+".0"
data_filename = data_filename+".lnd_xc_"+str(lxc)+".0.lnd_yc_"+str(lyc)+".0.lnd_wd_"+str(lwd)+".0.lnd_hg_"+str(lhg)+".0"
data_filename = data_filename+".wp_clat_"+wp_clat_str+".sst_clat_"+sst_clat_str
data_filename = data_filename+".nc"

top_dir     = home+"/Model/CESM/"
srcmod_dir  = top_dir+"mod_src/"
nmlmod_dir  = top_dir+"mod_nml/"

run_dir = "/glade/scratch/whannah/"
src_dir = home+"/Model/CESM/CESM_SRC/"
if cld == "ZM"     : src_dir = src_dir + "cesm1_2_2/"
#if cld == "ZM"     : src_dir = src_dir + "cesm1_1_2_spcam/"
if cld == "UNICON" : src_dir = src_dir + "UNICON_e13b7u1.8/"
if cld == "CLUBB"  : src_dir = src_dir + "clubbMG2_n36_cam5_3_08/"
#===================================================================================
#===================================================================================
os.system("cd "+top_dir)
case_dir  = top_dir+"Cases/"+case_name+"/"
cdcmd = "cd "+case_dir+" ; "
print
print "  case : "+case_name
print
#--------------------------
# Create new case
#--------------------------
if newcase == True:
    if cld == "ZM" :
        #compset_opt = "-user_compset 2000_CAM5_SLND_SICE_AQUAP_SROF_SGLC_SWAV"                 # equivalent to F_2000_CAM5_AQUAPLANET
        #compset_opt = "-compset F_2000_CAM5_AQUAPLANET"
        #compset_opt = "-compset F_2000_CAM5"
        #compset_opt = "-user_compset 2000_CAM5_DLND%NULL_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV"
        #compset_opt = "-user_compset 2000_CAM4_SLND_SICE_DOCN%DOM_SROF_SGLC_SWAV"
        compset_opt = "-user_compset 2000_CAM5_CLM45%CNDV_DICE%NULL_DOCN%DOM_SROF_SGLC_SWAV"
        #compset_opt = "-compset AQUA_ZM "
    if cld == "SP" :
        print "SP setup needs attention!"
        exit()
        #compset_opt = "-compset AQUA_SPCAM$_1mom "
    #compset_opt = compset_opt+" -compset_file /glade/u/home/whannah/Model/CESM/mod_compset/AQUA.xml"
    newcase_cmd = src_dir+"scripts/create_newcase"
    cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+"_"+res+" -mach  yellowstone "
    #cmd = newcase_cmd+" -case "+case_dir+"  "+compset_opt+"  -res  "+res+"_gx1v6 -mach  yellowstone "
    print cmd
    os.system(cmd)
    
    #os.system(cdcmd+"./xmlchange -file env_build.xml    -id COMPILER            -val gnu")
#--------------------------
# Configure the case
#--------------------------
if config == True:
    # set run-time variables
    start_date = "0000-01-01"
    os.system(cdcmd+"./xmlchange -file env_run.xml -id RUN_STARTDATE        -val "+start_date)
    os.system(cdcmd+"./xmlchange -file env_run.xml -id SSTICE_DATA_FILENAME -val "+data_filename)
    
    #os.system(cdcmd+"./xmlchange -file env_build.xml -id DEBUG  -val TRUE")
    
    # set processor number
    if False :
        ntask = 60
        nthrd =  1
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ATM -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_LND -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ICE -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_OCN -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_CPL -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_GLC -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_ROF -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTASKS_WAV -val "+str(ntask))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ATM -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_LND -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ICE -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_OCN -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_CPL -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_GLC -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_ROF -val "+str(nthrd))
        os.system(cdcmd+"./xmlchange -file env_mach_pes.xml -id NTHRDS_WAV -val "+str(nthrd))

    #------------------------------------------------
    # Copy the custom namelist file
    #------------------------------------------------
    os.system("cp "+nmlmod_dir+"user_nl_cam.nuna  "+case_dir+"user_nl_cam ")
    # We also need modified docn and dlnd namelists (for domain file paths)
    os.system("cp "+nmlmod_dir+"user_nl_docn.nuna "+case_dir+"user_nl_docn")
    os.system("cp "+nmlmod_dir+"user_nl_docn.nuna "+case_dir+"user_nl_dice")
    os.system("cp "+nmlmod_dir+"user_nl_dlnd.nuna "+case_dir+"user_nl_dlnd")
    # the cpl namelist has the orbital parameters
    os.system("cp "+nmlmod_dir+"user_nl_cpl.nuna  "+case_dir+"user_nl_cpl ")
    #------------------------------------------------
    # configure the case
    #------------------------------------------------
    os.system(cdcmd+"./cesm_setup -clean")
    os.system(cdcmd+"./cesm_setup ")    
#--------------------------
# Build the model
#--------------------------
if build == True:
    os.system(cdcmd+"./"+case_name+".clean_build")          # Clean previous build
    #os.system(cdcmd+"cp "+srcmod_dir+"* ./SourceMods/src.cam/")    # copy the modified source code
    os.system(cdcmd+"./"+case_name+".build")                # Build the model
#--------------------------
# Run the simulation
#--------------------------
if runsim == True:
    runfile = case_dir+case_name+".run"
    subfile = case_dir+case_name+".submit"

    os.system(cdcmd+"./xmlchange -file env_run.xml   -id STOP_N       -val "+str(ndays)) 
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id RESUBMIT     -val "+str(resub))
    os.system(cdcmd+"./xmlchange -file env_run.xml   -id CONTINUE_RUN -val "+cflag)
    
    os.system("sed -i '/#BSUB -P/ c\#BSUB -P P35081334' "+runfile)
    #os.system("sed -i '/#BSUB -n/ c\#BSUB -n 60'        "+runfile)
    os.system("sed -i '/#BSUB -W/ c\#BSUB -W 8:00'      "+runfile)
    
    # run the model
    os.system(cdcmd+subfile)

#--------------------------
#--------------------------
del case_dir
